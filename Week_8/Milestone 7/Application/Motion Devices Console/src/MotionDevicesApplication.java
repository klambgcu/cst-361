package com.motiondevices;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.sql.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.KeyStroke;
import javax.swing.UIManager;

/**
 * ---------------------------------------------------------------
 * Name      : Group 1 (Kelly Lamb, Brian Cantrell)
 * Date      : 2022-03-36
 * Class     : CST-361 Design Patterns with Java
 * Professor : Mohamed Mneimneh
 * Assignment: Milestone (Motion Devices)
 * Disclaimer: This is our own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Main Console Application for emulating motion sensor events
 * 2. Utilizes for the following design patterns:
 *    Factory Design Pattern
 *    Notifier/Observer Pattern
 *    REST API Patterns (Facade, DTO, DAO)
 * 3.
 * ---------------------------------------------------------------
 */

public class MotionDevicesApplication extends JFrame implements MouseMotionListener
{
	static final String NEWLINE = System.getProperty("line.separator");
	private java.util.Date startTime = new java.util.Date();
	private JTextArea textArea;
	private JPanel sensorGrid;
	private DataListFactory factory = new DataListFactory();

	public static void main(String[] args)
	{
		try
		{
			UIManager.setLookAndFeel( "javax.swing.plaf.metal.MetalLookAndFeel" );
			UIManager.put("swing.boldMetal", Boolean.FALSE);
			//UIManager.setLookAndFeel( "com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel" );
			//UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
		}
		catch ( Exception e )
		{
			e.printStackTrace();
		}

		javax.swing.SwingUtilities.invokeLater(new Runnable()
		{
			public void run()
			{
				MotionDevicesApplication app = new MotionDevicesApplication();
			}
		});
	}

	/**
	 * Create the GUI and show it.  For thread safety,
	 * this method should be invoked from the
	 * event-dispatching thread.
	 */
	private void initGUI()
	{
		// Create and set up the window.
		setJMenuBar(createMenu(this));

		// Add text area for visual event feedback
		textArea = new JTextArea();
		textArea.setEditable(false);
		JScrollPane scrollPane = new JScrollPane(textArea,
				JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
				JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		scrollPane.setPreferredSize(new Dimension(200, 75));
		add(scrollPane, BorderLayout.SOUTH);

		// Create empty sensor grid to begin
		sensorGrid = new JPanel(new GridLayout(5, 2, 30, 30));
		sensorGrid.setOpaque(true);
		sensorGrid.setBorder(BorderFactory.createEmptyBorder(20,20,20,20));

		// Load logo background image and add to the initial sensor grid
		ImageIcon logoImage = new ImageIcon(getClass().getResource("images/logo.jpg"));
		JLabel logoBackground = new JLabel(logoImage);
		add(logoBackground,  BorderLayout.NORTH);

		add(sensorGrid, BorderLayout.CENTER);

		// Set Window properties and display
		setPreferredSize(new Dimension(600, 600));
		setResizable(true);
		pack();
		setVisible(true);
	}

	private JMenuBar createMenu(JFrame frm)
	{
		final JFrame frame = frm;
		JMenuBar menuBar = new JMenuBar();

		/***********************/
		/* Menu Section - FILE */
		/***********************/
		JMenu menuFile = new JMenu("File");
		menuFile.setMnemonic('F');

		JMenuItem itemFileOpen = new JMenuItem("Open...");
		itemFileOpen.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O, InputEvent.CTRL_MASK));
		itemFileOpen.addActionListener
		(
			new ActionListener()
			{
				public void actionPerformed(ActionEvent event)
				{
					List<DTO> list = factory.getDataList(DataListFactory.DataListType.Users, 0);
					if (verifyAPIResponse(list, frame))
					{
						// Valid API response received
						DTO[] accounts = new DTO[list.size()];
						accounts = list.toArray(accounts);

						DTO account = (DTO)JOptionPane.showInputDialog(
											frame,
											"Choose device account to emulate",
											"Open Motion Devices Account",
											JOptionPane.QUESTION_MESSAGE,
											null,
											accounts,
											accounts[0]);

						if (account != null)
						{
							newSensorGrid(account, frame);
							return;
						}
					}
				}
			}
		);

		menuFile.add(itemFileOpen);

		menuFile.addSeparator();

		JMenuItem itemFileExit = new JMenuItem("Exit...");
		itemFileExit.setMnemonic('x');
		itemFileExit.addActionListener
		(
			new ActionListener()
			{
				public void actionPerformed(ActionEvent event)
				{
					System.exit(0);
				}
			}
		);
		menuFile.add(itemFileExit);

		menuBar.add(menuFile);

		/***********************/
		/* Menu Section - HELP */
		/***********************/
		JMenu menuHelp = new JMenu("Help");
		menuHelp.setMnemonic('H');

		JMenuItem itemHelpHelp = new JMenuItem("Help...");
		itemHelpHelp.setAccelerator( KeyStroke.getKeyStroke(KeyEvent.VK_F1, 0));
		itemHelpHelp.addActionListener
		(
			new ActionListener()
			{
				public void actionPerformed(ActionEvent event)
				{
					JOptionPane.showMessageDialog(frame,
												  "1. Make sure database is up and running on port 3306.\n" +
												  "2. Open dialog and choose device account to emulate.\n" +
												  "-----------------------------------------------------------------------------------------------\n" +
												  "Troubleshooting steps\n" +
												  "1. Make sure database is up and running on port 3306.\n" +
												  "2. Make sure you have the minimum java version installed correctly.\n" +
												  "3. Contact administrator.\n" +
												  "   Provide the application version, java version and chosen account.",
												  "Help",
												  JOptionPane.INFORMATION_MESSAGE);
				}
			}
		);

		menuHelp.add(itemHelpHelp);

		menuHelp.addSeparator();

		JMenuItem itemHelpAbout = new JMenuItem("About...");
		itemHelpAbout.setMnemonic('A');
		itemHelpAbout.addActionListener
		(
			new ActionListener()
			{
				public void actionPerformed(ActionEvent event)
				{
					JOptionPane.showMessageDialog(frame,
												  "Motion Devices 1.0.0\n" +
												  "Date: 02/22/2022\n" +
												  "Copyright: 2022 Group 1: Kelly Lamb, Brian Cantrell\n" +
												  "All Rights Reserved\n" +
												  "-------------------------------------------------\n" +
												  "Requires Java 1.6.0.18 or higher\n",
												  "About",
												  JOptionPane.INFORMATION_MESSAGE);
				}
			}
		);

		menuHelp.add(itemHelpAbout);

		menuBar.add(menuHelp);

		return menuBar;
	}

	public MotionDevicesApplication()
	{
		super("Motion Devices Console");
		initGUI();
		setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );
	}

	public void newSensorGrid(DTO account, JFrame frame)
	{
		getContentPane().remove(sensorGrid);
		sensorGrid = new JPanel(new GridLayout(5, 2, 30, 30));
		sensorGrid.setOpaque(true);
		sensorGrid.setBorder(BorderFactory.createEmptyBorder(20,20,20,20));

		if (null != account)
		{
			// Retrieve devices and create motion sensors
			List<DTO> list = factory.getDataList(DataListFactory.DataListType.Devices, account.getId());
			if (verifyAPIResponse(list, frame))
			{
				for (DTO item : list)
				{
					MotionSensor m = new MotionSensor(item.getId(), item.getDisplayName());
					sensorGrid.add(m);
					m.addMouseMotionListener(this);
				}

				add(sensorGrid, BorderLayout.CENTER);
				textArea.setText("Emulate Devices for Account: " + account + NEWLINE);
			}
			revalidate();
		}
	}

	public void mouseMoved(MouseEvent e)
	{
		LogEvent(e);
	}

	public void mouseDragged(MouseEvent e)
	{
		LogEvent(e);
	}

	public void LogEvent(MouseEvent e)
	{
		// Log Event if a second or more has passed
		java.util.Date eventTime = new java.util.Date();
		long seconds = (eventTime.getTime() - startTime.getTime())/1000;

		if (seconds >=1)
		{
			startTime = eventTime;
			int id = ((MotionSensor)e.getSource()).getId();
			String name = ((MotionSensor)e.getSource()).getName();
			createMotion(id);
			textArea.append("Motion Event on device: " + name + NEWLINE);
			textArea.setCaretPosition(textArea.getDocument().getLength());
		}
	}

	public void createMotion(int id)
	{
		try
		{
			Class.forName("com.mysql.cj.jdbc.Driver");
			Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/cst-361","root","root");
			PreparedStatement preparedStatement = connection.prepareStatement("insert into motions (device_id) values (?)", Statement.RETURN_GENERATED_KEYS);
			preparedStatement.setInt(1, id);
			preparedStatement.executeUpdate();
			connection.close();
		}
		catch(Exception e)
		{
			System.out.println(e);

			JOptionPane.showMessageDialog(null,
										  "Communication Failure: Database Insert Failure.\n" +
										  "------------------------------------------------------------------------\n" +
										  "Troubleshooting steps\n" +
										  "1. Contact administrator.\n" +
										  "2. System down. Confirm web application and database are up and running.\n",
										  "Error",
										  JOptionPane.ERROR_MESSAGE);

		}
	}

	public boolean verifyAPIResponse(List<DTO> list, JFrame frame)
	{
		if (list.size() == 0)
		{
			// Invalid API response received
			JOptionPane.showMessageDialog(frame,
										  "Communication Failure: REST API.\n" +
										  "------------------------------------------------------------------------\n" +
										  "Troubleshooting steps\n" +
										  "1. Contact administrator.\n" +
										  "2. System down. Confirm web application and database are up and running.\n",
										  "Error",
										  JOptionPane.ERROR_MESSAGE);

			return false;
		}

		return true;
	}
}
