package com.motiondevices;

/**
 * ---------------------------------------------------------------
 * Name      : Group 1 (Kelly Lamb, Brian Cantrell)
 * Date      : 2022-02-21
 * Class     : CST-361 Design Patterns with Java
 * Professor : Mohamed Mneimneh
 * Assignment: Milestone (Motion Devices)
 * Disclaimer: This is our own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Class defining a DTO for the console
 * 2. Used for shortened (facade) version of user and device info
 * 3.
 * ---------------------------------------------------------------
 */

public class Console
{
	private int id;
	private String displayName;

	public Console() {}

	/**
	 * @param id
	 * @param displayName
	 */
	public Console(int id, String displayName)
	{
		this.id = id;
		this.displayName = displayName;
	}

	/**
	 * @return the id
	 */
	public int getId()
	{
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id)
	{
		this.id = id;
	}

	/**
	 * @return the displayName
	 */
	public String getDisplayName()
	{
		return displayName;
	}

	/**
	 * @param displayName the displayName to set
	 */
	public void setDisplayName(String displayName)
	{
		this.displayName = displayName;
	}
}

