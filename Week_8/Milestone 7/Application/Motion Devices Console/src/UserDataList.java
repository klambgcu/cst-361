package com.motiondevices;

import java.util.ArrayList;
import java.util.List;

/**
 * ---------------------------------------------------------------
 * Name      : Group 1 (Kelly Lamb, Brian Cantrell)
 * Date      : 2022-03-26
 * Class     : CST-361 Design Patterns with Java
 * Professor : Mohamed Mneimneh
 * Assignment: Milestone (Motion Devices)
 * Disclaimer: This is our own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Class called from factory to provide DataList DTO
 * 2. Provides a list of users and count of devices to chose
 * 3.
 * ---------------------------------------------------------------
 */

public class UserDataList implements DataList
{
	private List<DTO> list = new ArrayList<DTO>();
	private DataListCallAPI api = new DataListCallAPI();

	public UserDataList()
	{
		//
		// Obtain user with devices information via REST API getusers
		//
		ResponseDataModel data = api.callAPI("http://localhost:8080/Milestone/rest/console/getusers");

		if (null != data && null != data.getData())
		{
			for (Console item : data.getData())
			{
				list.add( new DTO( item.getId(), item.getDisplayName() ) );
			}
		}
	}

	public List<DTO> getList()
	{
		return this.list;
	}
}
