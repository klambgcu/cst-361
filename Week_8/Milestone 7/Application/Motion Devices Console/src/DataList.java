package com.motiondevices;

import java.util.List;

/**
 * ---------------------------------------------------------------
 * Name      : Group 1 (Kelly Lamb, Brian Cantrell)
 * Date      : 2022-03-26
 * Class     : CST-361 Design Patterns with Java
 * Professor : Mohamed Mneimneh
 * Assignment: Milestone (Motion Devices)
 * Disclaimer: This is our own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Interface contract for DataList Factory
 * 2.
 * 3.
 * ---------------------------------------------------------------
 */

public interface DataList
{
	List<DTO> getList();
}
