-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Feb 26, 2022 at 07:30 PM
-- Server version: 5.7.24
-- PHP Version: 8.0.1

SET FOREIGN_KEY_CHECKS=0;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cst-361`
--
CREATE DATABASE IF NOT EXISTS `cst-361` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `cst-361`;

-- --------------------------------------------------------

--
-- Table structure for table `devices`
--
-- Creation: Feb 06, 2022 at 07:17 PM
--

DROP TABLE IF EXISTS `devices`;
CREATE TABLE IF NOT EXISTS `devices` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `CODE` int(11) NOT NULL,
  `NAME` varchar(100) NOT NULL,
  `USER_ID` int(11) NOT NULL,
  `ACTIVATED` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `USER_ID_IDX2` (`USER_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

--
-- Truncate table before insert `devices`
--

TRUNCATE TABLE `devices`;
--
-- Dumping data for table `devices`
--

INSERT INTO `devices` (`ID`, `CODE`, `NAME`, `USER_ID`, `ACTIVATED`) VALUES
(1, 1, 'FrontLeftCorner', 4, 1),
(2, 2, 'FrontRightCorner', 4, 1),
(3, 3, 'BackLeftCorner', 4, 1),
(4, 4, 'BackRightCorner', 4, 1),
(5, 5, 'FrontEntrance', 4, 1),
(6, 6, 'BackEntrance', 4, 1),
(7, 10, 'Driveway', 5, 1),
(8, 11, 'Front Door', 5, 1),
(9, 12, 'Back Door', 5, 1),
(13, 20, 'Driveway', 7, 1),
(14, 21, 'Front Door', 7, 1),
(15, 22, 'Back Door', 7, 1);

-- --------------------------------------------------------

--
-- Table structure for table `motions`
--
-- Creation: Feb 06, 2022 at 07:22 PM
--

DROP TABLE IF EXISTS `motions`;
CREATE TABLE IF NOT EXISTS `motions` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `DEVICE_ID` int(11) NOT NULL,
  `TRIGGERED_TIMESTAMP` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`),
  KEY `DEVICE_ID_IDX1` (`DEVICE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Truncate table before insert `motions`
--

TRUNCATE TABLE `motions`;
-- --------------------------------------------------------

--
-- Table structure for table `roles`
--
-- Creation: Feb 20, 2022 at 01:49 AM
--

DROP TABLE IF EXISTS `roles`;
CREATE TABLE IF NOT EXISTS `roles` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ROLENAME` varchar(45) NOT NULL,
  `DESCRIPTION` varchar(45) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

--
-- Truncate table before insert `roles`
--

TRUNCATE TABLE `roles`;
--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`ID`, `ROLENAME`, `DESCRIPTION`) VALUES
(1, 'Normal User', 'A Customer'),
(2, 'Installer', 'Device Installer'),
(3, 'Maintenance', 'Maintenance'),
(4, 'Admin', 'Administrator'),
(7, 'TESTROLENAME', 'TESTROLEDESCRIPTION');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--
-- Creation: Feb 20, 2022 at 02:01 AM
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `FIRSTNAME` varchar(50) NOT NULL,
  `LASTNAME` varchar(50) NOT NULL,
  `EMAIL` varchar(100) NOT NULL,
  `MOBILE` varchar(100) NOT NULL,
  `PASSWORD` varchar(250) NOT NULL,
  `ROLE_ID` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `EMAIL_UNIQ_IDX1` (`EMAIL`),
  KEY `USER_ROLE_ID_IDX` (`ROLE_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

--
-- Truncate table before insert `users`
--

TRUNCATE TABLE `users`;
--
-- Dumping data for table `users`
--

INSERT INTO `users` (`ID`, `FIRSTNAME`, `LASTNAME`, `EMAIL`, `MOBILE`, `PASSWORD`, `ROLE_ID`) VALUES
(1, 'Installer', 'Installer', 'installer@motiondevices.com', '(111) 111-1111', '11111111', 2),
(2, 'Maintenance', 'Maintenance', 'maintenance@motiondevices.com', '(111) 111-1111', '22222222', 3),
(3, 'Ad', 'min', 'admin@getjobs.com', '(333) 333-3333', '33333333', 4),
(4, 'Kelly', 'Lamb', 'kl@kl.com', '(562) 111-1111', '12345678', 1),
(5, 'Brian', 'Cantrell', 'bc@bc.com', '(602) 111-1111', '12345678', 1),
(7, 'Deena', 'Lamb', 'dl@dl.com', '(123) 123-1234', '11111111', 1),
(8, 'Ashley', 'Lamb', 'al@al.com', '(123) 123-1234', '11111111', 1),
(9, 'Test123', 'Test123', 'test123@test123.com', '(123) 123-1234', '11111111', 1);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `devices`
--
ALTER TABLE `devices`
  ADD CONSTRAINT `USERS_ID_FK1` FOREIGN KEY (`USER_ID`) REFERENCES `users` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `motions`
--
ALTER TABLE `motions`
  ADD CONSTRAINT `DEVICE_ID_FK1` FOREIGN KEY (`DEVICE_ID`) REFERENCES `devices` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `ROLE_ID` FOREIGN KEY (`ROLE_ID`) REFERENCES `roles` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;
SET FOREIGN_KEY_CHECKS=1;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
