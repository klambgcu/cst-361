package beans;  
import javax.annotation.PostConstruct;  
import java.io.Serializable;  
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import javax.interceptor.Interceptors;

import org.primefaces.model.chart.Axis;  
import org.primefaces.model.chart.AxisType;  
import org.primefaces.model.chart.CategoryAxis;  
import org.primefaces.model.chart.LineChartModel;  
import org.primefaces.model.chart.LineChartSeries;

import interceptors.LoggingInterceptor;  

@Named
@ViewScoped
@Interceptors(LoggingInterceptor.class)
public class ChartDataBean implements Serializable {  
	private static final long serialVersionUID = 5035740043427071064L;
	private LineChartModel drawArea;  

	@PostConstruct  
	public void init() {  
		createDrawArea();  
	}
	
	public LineChartModel getDrawArea() {  
		return drawArea;  
	}
	
	private void createDrawArea() {  
		drawArea = new LineChartModel();  
		LineChartSeries device = new LineChartSeries();  
		
		device.setFill(true);  
		device.setLabel("Device 1");  
		device.set("15:54", 3);  
		device.set("15:55", 1);  
		device.set("15:56", 3);  
		device.set("15:57", 10);  
		device.set("15:58", 4);  
		 
		drawArea.addSeries(device);  
		drawArea.setTitle("Motion Report");  
		drawArea.setLegendPosition("ne");  
		drawArea.setStacked(true);  
		drawArea.setShowPointLabels(true);  
		
		Axis xAxis = new CategoryAxis("Time Stamp");  
		
		drawArea.getAxes().put(AxisType.X, xAxis);  
		
		Axis yAxis = drawArea.getAxis(AxisType.Y);  
		
		yAxis.setLabel("Motion Tics");  
		yAxis.setMin(0); 

		// Set to highest motion count from devices
		yAxis.setMax(30);  
	}  
}  