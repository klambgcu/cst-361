package beans;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 * ---------------------------------------------------------------
 * Name      : Group 1 (Kelly Lamb, Brian Cantrell)
 * Date      : 2022-02-14
 * Class     : CST-361 Design Patterns with Java
 * Professor : Mohamed Mneimneh
 * Assignment: Milestone (Motion Devices)
 * Disclaimer: This is our own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Class defining a motion sensor device
 * 2.
 * 3.
 * ---------------------------------------------------------------
 */

@ManagedBean
@ViewScoped
public class Device
{
	private int id;
	private int code;
	private String name;
	private int user_id;
	private boolean activated;
	
	/**
	 * Default Constructor
	 */
	public Device() {}

	/**
	 * @param id integer key
	 * @param code integer code for device
	 * @param name String name of device
	 * @param user_id integer user_id account associated to device
	 * @param activated boolean active = true (1), inactive = false (0)
	 */
	public Device(int id, int code, String name, int user_id, boolean activated)
	{
		this.id = id;
		this.code = code;
		this.name = name;
		this.user_id = user_id;
		this.activated = activated;
	}

	/**
	 * @return the id
	 */
	public int getId()
	{
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id)
	{
		this.id = id;
	}

	/**
	 * @return the code
	 */
	public int getCode()
	{
		return code;
	}

	/**
	 * @param code the code to set
	 */
	public void setCode(int code)
	{
		this.code = code;
	}

	/**
	 * @return the name
	 */
	public String getName()
	{
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name)
	{
		this.name = name;
	}

	/**
	 * @return the user_id
	 */
	public int getUser_id()
	{
		return user_id;
	}

	/**
	 * @param user_id the user_id to set
	 */
	public void setUser_id(int user_id)
	{
		this.user_id = user_id;
	}

	/**
	 * @return the activated
	 */
	public boolean isActivated()
	{
		return activated;
	}

	/**
	 * @param activated the activated to set
	 */
	public void setActivated(boolean activated)
	{
		this.activated = activated;
	}
}
