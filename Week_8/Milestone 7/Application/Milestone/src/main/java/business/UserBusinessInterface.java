package business;

import java.util.List;

import beans.User;

/**
 * ---------------------------------------------------------------
 * Name      : Group 1 (Kelly Lamb, Brian Cantrell)
 * Date      : 2022-02-14
 * Class     : CST-361 Design Patterns with Java
 * Professor : Mohamed Mneimneh
 * Assignment: Milestone (Motion Devices)
 * Disclaimer: This is our own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Interface to house all business processes for users
 * 2. 
 * 3.
 * ---------------------------------------------------------------
 */

public interface UserBusinessInterface
{	   
    /**
     * 
     * @return List : All users from the database
     */
    public List<User> readAllUsers();
    
    /**
     * 
     * @param user : User to insert into the database
     * @return boolean: true(successful), false otherwise
     */
    public boolean createUser(User user);
    
    /**
     * 
     * @param id: Integer ID (Key) to select user
     * @return User: User object if found otherwise null
     */
    public User selectUserById(int id);
    
    /**
     * 
     * @param user : User model to delete from database (ID)
     * @return boolean: true(successful), false otherwise
     */
    public boolean deleteUser(User user);
    
    /**
     * 
     * @param user : User object to update
     * @return boolean: true(successful), false otherwise
     */
    public boolean updateUser(User user);

    /**
     * 
     * @param email : String name to search for user
     * @param password : String name to search for user
     * @return User: User object if found otherwise null
     */
    public User selectUserByEmailPassword(String email, String password);
    
    /**
     * 
     * @param email : email should be unique - verify
     * @return boolean: true(exists), false otherwise
     */
    public boolean doesEmailExist(String email);
    
}