package business;

import java.util.List;

import beans.Motion;

/**
 * ---------------------------------------------------------------
 * Name      : Group 1 (Kelly Lamb, Brian Cantrell)
 * Date      : 2022-02-14
 * Class     : CST-361 Design Patterns with Java
 * Professor : Mohamed Mneimneh
 * Assignment: Milestone (Motion Motions)
 * Disclaimer: This is our own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Interface to house all business processes for motions
 * 2. 
 * 3.
 * ---------------------------------------------------------------
 */

public interface MotionBusinessInterface
{
    /**
     * 
     * @return List : All motions from the database
     */
    public List<Motion> readAllMotions();
    
    /**
     * 
     * @param motion : Motion to insert into the database
     * @return boolean: true(successful), false otherwise
     */
    public boolean createMotion(Motion motion);
    
    /**
     * 
     * @param id: Integer ID (Key) to select motion
     * @return Motion: User object if found otherwise null
     */
    public Motion selectMotionById(int id);
    
    /**
     * 
     * @param motion : Motion model to delete from database (ID)
     * @return boolean: true(successful), false otherwise
     */
    public boolean deleteMotion(Motion motion);
    
    /**
     * 
     * @param motion : Motion object to update
     * @return boolean: true(successful), false otherwise
     */
    public boolean updateMotion(Motion motion);

    /**
     * 
     * @param device_id : integer id for device events to return
     * @return List : All motions for specified device from the database
     */
	public List<Motion> readAllMotionsForDeviceId(int device_id);
}
