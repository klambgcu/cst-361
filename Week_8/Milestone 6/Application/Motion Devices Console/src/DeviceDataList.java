package com.motiondevices;

import java.util.ArrayList;
import java.util.List;

/**
 * ---------------------------------------------------------------
 * Name      : Group 1 (Kelly Lamb, Brian Cantrell)
 * Date      : 2022-03-26
 * Class     : CST-361 Design Patterns with Java
 * Professor : Mohamed Mneimneh
 * Assignment: Milestone (Motion Devices)
 * Disclaimer: This is our own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Class called from factory to provide DataList DTO
 * 2. Provides a list of devices and their id for console display
 * 3.
 * ---------------------------------------------------------------
 */

public class DeviceDataList implements DataList
{
	private List<DTO> list = new ArrayList<DTO>();
	private DataListCallAPI api = new DataListCallAPI();

	public DeviceDataList(int user_id)
	{
		//
		// Obtain device information for specific user via REST API getdevices/{id}
		//
		ResponseDataModel data = api.callAPI("http://localhost:8080/Milestone/rest/console/getdevices/" + user_id);

		for (Console item : data.getData())
		{
			list.add( new DTO( item.getId(), item.getDisplayName() ) );
		}
	}

	public List<DTO> getList()
	{
		return this.list;
	}
}
