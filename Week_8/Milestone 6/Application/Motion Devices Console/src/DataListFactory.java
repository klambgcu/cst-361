package com.motiondevices;

import java.util.List;

/**
 * ---------------------------------------------------------------
 * Name      : Group 1 (Kelly Lamb, Brian Cantrell)
 * Date      : 2022-03-26
 * Class     : CST-361 Design Patterns with Java
 * Professor : Mohamed Mneimneh
 * Assignment: Milestone (Motion Devices)
 * Disclaimer: This is our own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Factory to produce Data List of user or device objects
 * 2. Utilizes a Factory Pattern design
 * 3.
 * ---------------------------------------------------------------
 */

public class DataListFactory
{
	public static enum DataListType
	{
		Users,
		Devices
	}

	public List<DTO> getDataList(DataListType type, int id)
	{
		DataList obj = null;

		switch (type)
		{
			case Users :
			{
				obj = new UserDataList();
				break;
			}
			case Devices :
			{
				obj =  new DeviceDataList(id);
				break;
			}
		}

		return obj.getList();
	}
}
