package com.motiondevices;

/**
 * ---------------------------------------------------------------
 * Name      : Group 1 (Kelly Lamb, Brian Cantrell)
 * Date      : 2022-03-26
 * Class     : CST-361 Design Patterns with Java
 * Professor : Mohamed Mneimneh
 * Assignment: Milestone (Motion Devices)
 * Disclaimer: This is our own work
 * ---------------------------------------------------------------
 * Description:
 * 1. DTO (Data Transfer Object) - handle commication info
 *    provided by REST API from web application.
 * 2.
 * 3.
 * ---------------------------------------------------------------
 */

public class DTO
{
	private int id;
	private String displayName;

	public DTO(int id, String displayName)
	{
		this.id = id;
		this.displayName = displayName;
	}

	public int getId()
	{
		return this.id;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public String getDisplayName()
	{
		return this.displayName;
	}

	public void setDisplayName(String displayName)
	{
		this.displayName = displayName;
	}

	@Override
	public String toString()
	{
		return this.displayName;
	}
}
