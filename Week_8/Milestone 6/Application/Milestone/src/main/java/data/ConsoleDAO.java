package data;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;

import beans.Console;
import interceptors.LoggingInterceptor;
import util.DatabaseException;

/**
 * ---------------------------------------------------------------
 * Name      : Group 1 (Kelly Lamb, Brian Cantrell)
 * Date      : 2022-02-21
 * Class     : CST-361 Design Patterns with Java
 * Professor : Mohamed Mneimneh
 * Assignment: Milestone (Motion Devices)
 * Disclaimer: This is our own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Class defining a DAO for Console support
 * 2. users with devices activated
 * 3. activated devices assigned to users
 * ---------------------------------------------------------------
 */

/**
 * Session Bean implementation
 */
@Stateless
@LocalBean
@Interceptors(LoggingInterceptor.class)
public class ConsoleDAO extends DatabaseConfig
{
	//
	// Define query strings
	//
	private final static String SQL_SELECT_USERS_WITH_DEVICES	   = "select u.id, u.firstname as name, count(1) as amount from users u, devices d where u.role_id = 1 and u.id = d.user_id and d.activated = true group by u.id, u.firstname order by u.id";
	private final static String SQL_SELECT_USERS_DEVICE_BY_USER_ID = "select id, name from devices where user_id = ? and activated = 1 order by id";

	public ConsoleDAO() {}
	
	public List<Console> getUsersWithDevices()
	{
		List<Console> list = new ArrayList<Console>();
		
		try (Connection connection = getConnection();
			PreparedStatement preparedStatement = connection.prepareStatement(SQL_SELECT_USERS_WITH_DEVICES);)
		{
			ResultSet rs = preparedStatement.executeQuery();

			while (rs.next())
			{
				int user_id = rs.getInt("id");
				String name = rs.getString("name");
				int amount  = rs.getInt("amount");
				String displayName = name + " (" + amount + ")";
				list.add( new Console(user_id, displayName) );
			}
		}
		catch (SQLException e)
		{
			e.printStackTrace();
			throw new DatabaseException(e);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			throw new DatabaseException(e);
		}

		return list;
	}
	
	public List<Console> getUsersWithDevices(int user_id)
	{
		List<Console> list = new ArrayList<Console>();
		
		try (Connection connection = getConnection();
			PreparedStatement preparedStatement = connection.prepareStatement(SQL_SELECT_USERS_DEVICE_BY_USER_ID);)
		{
			preparedStatement.setInt(1, user_id);

			ResultSet rs = preparedStatement.executeQuery();

			while (rs.next())
			{
				int device_id = rs.getInt("id");
				String name   = rs.getString("name");
				String displayName = name + " (" + device_id + ")";
				list.add( new Console(device_id, displayName) );
			}
		}
		catch (SQLException e)
		{
			e.printStackTrace();
			throw new DatabaseException(e);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			throw new DatabaseException(e);
		}

		return list;
	}
}
