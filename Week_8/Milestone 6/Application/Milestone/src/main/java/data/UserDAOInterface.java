package data;

/**
 * ---------------------------------------------------------------
 * Name      : Group 1 (Kelly Lamb, Brian Cantrell)
 * Date      : 2022-02-14
 * Class     : CST-361 Design Patterns with Java
 * Professor : Mohamed Mneimneh
 * Assignment: Milestone (Motion Devices)
 * Disclaimer: This is our own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Generic interface for database access (consistent naming)
 * 2. Extend as necessary
 * 3.
 * ---------------------------------------------------------------
 */

public interface UserDAOInterface<T> extends DataAccessInterface<T>
{
	public T getByEmailPassword(String email, String password);
	public boolean emailExists(String email);
}
