package data;

import java.util.List;

import beans.Console;
import beans.Device;
import beans.Motion;
import beans.Role;
import beans.User;

/**
 * ---------------------------------------------------------------
 * Name      : Group 1 (Kelly Lamb, Brian Cantrell)
 * Date      : 2022-02-14
 * Class     : CST-361 Design Patterns with Java
 * Professor : Mohamed Mneimneh
 * Assignment: Milestone (Motion Devices)
 * Disclaimer: This is our own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Interface to house all database CRUD exposed methods
 * 2. 
 * 3.
 * ---------------------------------------------------------------
 */

public interface DatabaseInterface
{
    //-----------------------------------------------------
    // All exposed methods for the User DAO
    //-----------------------------------------------------
    
    /**
     * 
     * @return List : All users from the database
     */
    public List<User> readAllUsers();
    
    /**
     * 
     * @param user : User to insert into the database
     * @return boolean: true(successful), false otherwise
     */
    public boolean createUser(User user);
    
    /**
     * 
     * @param id: Integer ID (Key) to select user
     * @return User: User object if found otherwise null
     */
    public User selectUserById(int id);
    
    /**
     * 
     * @param user : User model to delete from database (ID)
     * @return boolean: true(successful), false otherwise
     */
    public boolean deleteUser(User user);
    
    /**
     * 
     * @param user : User object to update
     * @return boolean: true(successful), false otherwise
     */
    public boolean updateUser(User user);

    /**
     * 
     * @param email : String name to search for user
     * @param password : String name to search for user
     * @return User: User object if found otherwise null
     */
    public User selectUserByEmailPassword(String email, String password);
    
    /**
     * 
     * @param email : email should be unique - verify
     * @return boolean: true(exists), false otherwise
     */
    public boolean doesEmailExist(String email);
    
    //-----------------------------------------------------
    // All exposed methods for the Role DAO
    //-----------------------------------------------------
    /**
     * 
     * @return List : All roles from the database
     */
    public List<Role> readAllRoles();
    
    /**
     * 
     * @param role : Role to insert into the database
     * @return boolean: true(successful), false otherwise
     */
    public boolean createRole(Role role);
    
    /**
     * 
     * @param id: Integer ID (Key) to select role
     * @return Role: User object if found otherwise null
     */
    public Role selectRoleById(int id);
    
    /**
     * 
     * @param role : Role model to delete from database (ID)
     * @return boolean: true(successful), false otherwise
     */
    public boolean deleteRole(Role role);
    
    /**
     * 
     * @param role : Role object to update
     * @return boolean: true(successful), false otherwise
     */
    public boolean updateRole(Role role);

    
    //-----------------------------------------------------
    // All exposed methods for the Device DAO
    //-----------------------------------------------------
    /**
     * 
     * @return List : All devices from the database
     */
    public List<Device> readAllDevices();
    
    /**
     * 
     * @param device : Device to insert into the database
     * @return boolean: true(successful), false otherwise
     */
    public boolean createDevice(Device device);
    
    /**
     * 
     * @param id: Integer ID (Key) to select device
     * @return Device: User object if found otherwise null
     */
    public Device selectDeviceById(int id);
    
    /**
     * 
     * @param device : Device model to delete from database (ID)
     * @return boolean: true(successful), false otherwise
     */
    public boolean deleteDevice(Device device);
    
    /**
     * 
     * @param device : Device object to update
     * @return boolean: true(successful), false otherwise
     */
    public boolean updateDevice(Device device);

    
    //-----------------------------------------------------
    // All exposed methods for the Motion DAO
    //-----------------------------------------------------
    /**
     * 
     * @return List : All motions from the database
     */
    public List<Motion> readAllMotions();
    
    /**
     * 
     * @param motion : Motion to insert into the database
     * @return boolean: true(successful), false otherwise
     */
    public boolean createMotion(Motion motion);
    
    /**
     * 
     * @param id: Integer ID (Key) to select motion
     * @return Motion: User object if found otherwise null
     */
    public Motion selectMotionById(int id);
    
    /**
     * 
     * @param motion : Motion model to delete from database (ID)
     * @return boolean: true(successful), false otherwise
     */
    public boolean deleteMotion(Motion motion);
    
    /**
     * 
     * @param motion : Motion object to update
     * @return boolean: true(successful), false otherwise
     */
    public boolean updateMotion(Motion motion);
    
    /**
     * 
     * @param device_id : integer id for device events to return
     * @return List : All motions for specified device from the database
     */
	public List<Motion> readAllMotionsForDeviceId(int device_id);

    
    //-----------------------------------------------------
    // All exposed methods for the Console DAO
    // A specialized DAO to handle communication to the 
    // console application.
    //-----------------------------------------------------
    
    /**
     * 
     * @return List : All users that have devices assigned and activated database
     */
	List<Console> getUsersWithDevices();
	
    /**
     * @param user_id : Integer id for user
     * @return List : All activated devices for specific user from the database
     */
	List<Console> getUsersWithDevices(int user_id);
}
