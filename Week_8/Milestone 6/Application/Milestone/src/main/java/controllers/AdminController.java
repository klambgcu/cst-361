package controllers;

import java.io.Serializable;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import javax.interceptor.Interceptors;

import beans.Device;
import beans.User;
import business.CacheDevice;
import business.UserBusinessInterface;
import interceptors.LoggingInterceptor;

/**
 * ---------------------------------------------------------------
 * Name      : Group 1 (Kelly Lamb, Brian Cantrell)
 * Date      : 2022-03-26
 * Class     : CST-361 Design Patterns with Java
 * Professor : Mohamed Mneimneh
 * Assignment: Milestone (Motion Devices)
 * Disclaimer: This is our own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Controller for Administrator modules (Users, Devices)
 * 2. 
 * 3.
 * ---------------------------------------------------------------
 */

@Named
@ViewScoped
@Interceptors(LoggingInterceptor.class)
public class AdminController implements Serializable
{
	private static final long serialVersionUID = -1412530581012441421L;

	@EJB CacheDevice cache;

	@EJB UserBusinessInterface userService;
	
	public AdminController() {}
	
	public String getDeviceListing()
	{
		// Get current user - obtain devices for the specific user
		User user = ((User)FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("user"));

		// Reject unauthorized access - should be tied to actual roles
		if (user.getRole_id() < 2) return "login.xhtml";
		
		List<Device> devices = cache.getAllDevices();
		FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("devices", devices);
		
		return "adminDeviceModule.xhtml";
	}

	public String getUserListing()
	{
		// Get current user - obtain devices for the specific user
		User user = ((User)FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("user"));

		// Reject unauthorized access - should be tied to actual roles
		if (user.getRole_id() < 2) return "login.xhtml";
		
		List<User> users = userService.readAllUsers();
		FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("users", users);
		
		return "adminUserModule.xhtml";
	}

}
