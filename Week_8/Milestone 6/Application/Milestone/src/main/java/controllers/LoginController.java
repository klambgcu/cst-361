package controllers;

import java.io.Serializable;

import javax.ejb.EJB;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import javax.interceptor.Interceptors;

import beans.User;
import business.UserBusinessInterface;
import interceptors.LoggingInterceptor;

/**
 * ---------------------------------------------------------------
 * Name      : Group 1 (Kelly Lamb, Brian Cantrell)
 * Date      : 2022-02-14
 * Class     : CST-361 Design Patterns with Java
 * Professor : Mohamed Mneimneh
 * Assignment: Milestone (Motion Devices)
 * Disclaimer: This is our own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Controller for application login and registration
 * 2. 
 * 3.
 * ---------------------------------------------------------------
 */

@Named
@ViewScoped
@Interceptors(LoggingInterceptor.class)
public class LoginController implements Serializable
{
	private static final long serialVersionUID = -5111298267383579897L;
	
	@EJB UserBusinessInterface service;
	
	/**
	 * Handle user account login
	 * @param user User model
	 * @return String web page
	 */
	public String login(User user)
	{
		System.out.println("LoginController:login - entering");
		System.out.println(user.getEmail() + " " + user.getPassword());

		//UserBusinessInterface service = new UserBusinessService();

		try
		{
			User validUser = service.selectUserByEmailPassword(user.getEmail(), user.getPassword());
	
			if (null == validUser)
			{
				// Credentials are not valid. Put info back into request and message to inform user
				FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("user", user);
				String message = "Login Unsuccessful. The credentials are not correct. Please check and try again.";
				FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("message", message);

				// Clear user from the session
				FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("user");
			}
			else
			{
				// Credentials are valid. Put info into session and message to inform user
				FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("user", validUser);
				String message = "Login Successful.";
				FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("message", message);			
				
				// Add user to the session
				FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("user", validUser);
			}
		}
		catch (Exception e)
		{
			// Login Failed - Database Error.
			FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("user", user);
			String message = "Login Unsuccessful. An unexpected error occurred. Please contact administrator.";
			FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("message", message);			

			// Clear user from the session
			FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("user");

			System.out.println("LoginController:login - error occurred.");
			e.printStackTrace();		
		}
		
		System.out.println("LoginController:login - exiting");

		return "login.xhtml";
	}
	
	/**
	 * Handle User Account Registration
	 * @param user User model
	 * @return String web page
	 */
	public String register(User user)
	{
		System.out.println("LoginController:register - entering");
		System.out.println(user.getFirstname() + " " + user.getLastname());
		
		// Default user role to 1 = Normal User
		user.setRole_id(1);

		//UserBusinessInterface service = new UserBusinessService();
		
		try
		{
			if ( service.doesEmailExist(user.getEmail()) )
			{
				// Email already exists in database - inform user
				FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("user", user);
				String message = "Registration Unsuccessful. Email already exists. Please check and try again.";
				FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("message", message);
			}
			else if (service.createUser(user))
			{
				// Registration successfully
				FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("user", user);
				String message = "Welcome! Registration Successful. Please login.";
				FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("message", message);						
			}
			else
			{
				// Registration Failed.
				FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("user", user);
				String message = "Registration Unsuccessful. Please check and try again or contact administrator.";
				FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("message", message);			
			}
		}
		catch (Exception e)
		{
			// Registration Failed.
			FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("user", user);
			String message = "Registration Unsuccessful. An unexpected error occurred. Please contact administrator.";
			FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("message", message);			
			System.out.println("LoginController:register - error occurred.");
			e.printStackTrace();
		}

		System.out.println("LoginController:register - exiting");
		FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("user", user);

		return "register.xhtml";
	}
	
	public String logout()
	{
		// Clear user from the session
		FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("user");

        // Invalidate session (i.e. user not valid anymore)
        FacesContext.getCurrentInstance().getExternalContext().invalidateSession();

        return "index.xhtml";
	}
}
