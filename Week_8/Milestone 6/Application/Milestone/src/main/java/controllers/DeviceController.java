package controllers;

import java.io.Serializable;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import javax.interceptor.Interceptors;

import beans.Device;
import beans.Motion;
import beans.User;
import business.CacheDevice;
import business.MotionBusinessInterface;
import interceptors.LoggingInterceptor;

/**
 * ---------------------------------------------------------------
 * Name      : Group 1 (Kelly Lamb, Brian Cantrell)
 * Date      : 2022-02-14
 * Class     : CST-361 Design Patterns with Java
 * Professor : Mohamed Mneimneh
 * Assignment: Milestone (Motion Devices)
 * Disclaimer: This is our own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Controller for application login and registration
 * 2. 
 * 3.
 * ---------------------------------------------------------------
 */

@Named
@ViewScoped
@Interceptors(LoggingInterceptor.class)
public class DeviceController implements Serializable
{
	private static final long serialVersionUID = -1412530581012441421L;

	@EJB CacheDevice cache;
	@EJB MotionBusinessInterface motionService;
	
	public DeviceController() {}
	
	public String getMotions(Device device)
	{
		
		System.out.println("DeviceController::getMotions - entering");
		int device_id = (null == device) ? 5 : device.getId();
		System.out.println("Device ID: " + device_id);
		
		List<Motion> motions = motionService.readAllMotionsForDeviceId(device_id);
		FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("motions", motions);

		// System.out.println("Count: " + motions.size());
		
		System.out.println("DeviceController::getMotions - exiting");
		return "motions.xhtml";
	}

	public String getDeviceListing()
	{
		System.out.println("DeviceController::getDeviceListing - entering");

		// Get current user - obtain devices for the specific user
		User user = ((User)FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("user"));

		List<Device> devices = cache.getAllDevicesForUser(user.getId());
		FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("devices", devices);
		
		System.out.println("DeviceController::getDeviceListing - exiting");
		return "devices.xhtml";
	}
	
}
