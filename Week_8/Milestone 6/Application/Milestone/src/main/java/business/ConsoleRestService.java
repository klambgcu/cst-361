package business;

import java.util.List;

import javax.ejb.EJB;
import javax.interceptor.Interceptors;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import beans.Console;
import beans.ResponseDataModel;
import interceptors.LoggingInterceptor;

/**
 * ---------------------------------------------------------------
 * Name      : Group 1 (Kelly Lamb, Brian Cantrell)
 * Date      : 2022-02-21
 * Class     : CST-361 Design Patterns with Java
 * Professor : Mohamed Mneimneh
 * Assignment: Milestone (Motion Devices)
 * Disclaimer: This is our own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Class defining a RESTful service for Console information
 * 2. JSON getusers
 * 3. JSON getdevices/id
 * ---------------------------------------------------------------
 */

@Path("/console")
@Interceptors(LoggingInterceptor.class)
public class ConsoleRestService
{
	@EJB ConsoleBusinessInterface service;
	
	@GET
	@Path("/getusers")
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseDataModel getUsers()
	{
		System.out.println("RESTAPI: ConsoleRestService::getUsers-Entering");
		
		List<Console> data = null;
		int status = 0;
		String message = "";
		
		try
		{
			data = service.getUsersWithDevices();
			
			if (data.size() == 0)
			{
				status = 200;
				message = "No users assigned devices.";
			}
		}
		catch (Exception e)
		{
			System.out.println(e.getMessage());			
			status = 500;
			message = "An error has occurred. Please contact administrator.";
		}
		
		System.out.println("RESTAPI: ConsoleRestService::getUsers-Exiting");
		return new ResponseDataModel(status, message, data);
	}
	
	@GET
	@Path("/getdevices/{id}")
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseDataModel getUserDevices( @PathParam("id") int id )
	{
		System.out.println("RESTAPI: ConsoleRestService::getUserDevices-Entering");
		
		List<Console> data = null;
		int status = 0;
		String message = "";
		
		try
		{
			data = service.getUsersWithDevices(id);
			
			if (data.size() == 0)
			{
				status = 404;
				message = "No Devices found for user id = " + id + ".";
			}
		}
		catch (Exception e)
		{
			System.out.println(e.getMessage());			
			status = 500;
			message = "An error has occurred. Please contact administrator.";
		}
		
		System.out.println("RESTAPI: ConsoleRestService::getUserDevices-Exiting");
		return new ResponseDataModel(status, message, data);
	}
}
