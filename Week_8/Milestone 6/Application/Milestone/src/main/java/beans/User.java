package beans;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;

/**
 * ---------------------------------------------------------------
 * Name      : Group 1 (Kelly Lamb, Brian Cantrell)
 * Date      : 2022-02-14
 * Class     : CST-361 Design Patterns with Java
 * Professor : Mohamed Mneimneh
 * Assignment: Milestone (Motion Devices)
 * Disclaimer: This is our own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Class defining a application user
 * 2. Role: 1=Normal, 2=Installer, 3=Maintenance, 4=Administrator
 * 3.
 * ---------------------------------------------------------------
 */

@ManagedBean
public class User
{
	private int id;
	private String firstname;
	private String lastname;
	private String email;
	private String mobile;
	private String password;
	private int role_id;
	
	/**
	 * Default Constructor
	 */
	public User() {}
	
	/**
	 * Non-Default Constructor
	 * @param id integer key
	 * @param firstname String user first name
	 * @param lastname String user last name
	 * @param email String user email address
	 * @param mobile String user phone number
	 * @param password String user password
	 * @param role_id integer user role key 
	 */
	public User(int id, String firstname, String lastname, String email, String mobile, String password, int role_id)
	{
		this.id = id;
		this.firstname = firstname;
		this.lastname = lastname;
		this.email = email;
		this.mobile = mobile;
		this.password = password;
		this.role_id = role_id;
	}

	/**
	 * @return the id
	 */
	public int getId()
	{
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id)
	{
		this.id = id;
	}

	/**
	 * @return the firstname
	 */
	public String getFirstname()
	{
		return firstname;
	}

	/**
	 * @param firstname the firstname to set
	 */
	public void setFirstname(String firstname)
	{
		this.firstname = firstname;
	}

	/**
	 * @return the lastname
	 */
	public String getLastname()
	{
		return lastname;
	}

	/**
	 * @param lastname the lastname to set
	 */
	public void setLastname(String lastname)
	{
		this.lastname = lastname;
	}

	/**
	 * @return the email
	 */
	public String getEmail()
	{
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email)
	{
		this.email = email;
	}

	/**
	 * @return the mobile
	 */
	public String getMobile()
	{
		return mobile;
	}

	/**
	 * @param mobile the mobile to set
	 */
	public void setMobile(String mobile)
	{
		this.mobile = mobile;
	}

	/**
	 * @return the password
	 */
	public String getPassword()
	{
		return password;
	}

	/**
	 * @param password the password to set
	 */
	public void setPassword(String password)
	{
		this.password = password;
	}

	/**
	 * @return the role_id
	 */
	public int getRole_id()
	{
		return role_id;
	}

	/**
	 * @param role_id the role_id to set
	 */
	public void setRole_id(int role_id)
	{
		this.role_id = role_id;
	}
}
