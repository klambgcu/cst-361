package beans;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 * ---------------------------------------------------------------
 * Name      : Group 1 (Kelly Lamb, Brian Cantrell)
 * Date      : 2022-02-14
 * Class     : CST-361 Design Patterns with Java
 * Professor : Mohamed Mneimneh
 * Assignment: Milestone (Motion Devices)
 * Disclaimer: This is our own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Class defining a application user role
 * 2. Role: 1=Normal, 2=Installer, 3=Maintenance, 4=Administrator
 * 3.
 * ---------------------------------------------------------------
 */

@ManagedBean
@ViewScoped
public class Role
{
	private int id;
	private String rolename;
	private String description;
	
	/**
	 * Default Constructor
	 */
	public Role() {}

	/**
	 * @param id integer key
	 * @param rolename String name of role
	 * @param description String description of role
	 */
	public Role(int id, String rolename, String description)
	{
		this.id = id;
		this.rolename = rolename;
		this.description = description;
	}

	/**
	 * @return the id
	 */
	public int getId()
	{
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id)
	{
		this.id = id;
	}

	/**
	 * @return the rolename
	 */
	public String getRolename()
	{
		return rolename;
	}

	/**
	 * @param rolename the rolename to set
	 */
	public void setRolename(String rolename)
	{
		this.rolename = rolename;
	}

	/**
	 * @return the description
	 */
	public String getDescription()
	{
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description)
	{
		this.description = description;
	}
}
