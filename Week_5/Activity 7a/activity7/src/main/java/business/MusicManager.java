package business;

import beans.Album;
import beans.Track;
import data.DataAccessInterface;
import util.AlbumNotFoundException;
import util.TracksNotFoundException;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Local;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;

/**
 * ---------------------------------------------------------------
 * Name      : Kelly Lamb
 * Date      : 2022-02-14
 * Class     : CST-361 Design Patterns with Java
 * Professor : Mohamed Mneimneh
 * Assignment: Activity 4
 * Disclaimer: This is our own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Concrete class to manage music albums track information
 * 2.
 * 3.
 * ---------------------------------------------------------------
 */

@Stateless
@Local(MusicManagerInterface.class)
@LocalBean
public class MusicManager implements MusicManagerInterface
{	
	@EJB 
	DataAccessInterface<Album> dao;
	
	@Inject
	TrackFinderInterface tf;
	
	/**
     * Default constructor. 
     */
    public MusicManager()
    {
    }

	/**
     * @see MusicManagerInterface#addAlbum(Album)
     */
    public Album addAlbum(Album model) throws TracksNotFoundException
    {
    	List<Track> list = tf.getTracks(model);
    	if (null == list) throw new TracksNotFoundException();
    	model.setTracks(list);

    	dao.create(model);
    	model = dao.findBy(model);

    	return model;
    }

    public Album getAlbum(Album model) throws AlbumNotFoundException
    {
    	Album result = dao.findBy(model);
    	if (null == result) throw new AlbumNotFoundException();
    	
    	return result;
    }
	
}
