package beans;

/**
 * ---------------------------------------------------------------
 * Name      : Kelly Lamb
 * Date      : 2022-02-14
 * Class     : CST-361 Design Patterns with Java
 * Professor : Mohamed Mneimneh
 * Assignment: Activity 4
 * Disclaimer: This is our own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Class defining a track for a music album
 * 2.
 * 3.
 * ---------------------------------------------------------------
 */

public class Track
{
	private String title;
	private int number;

	/**
	 * Default Constructor
	 */
	public Track()
	{
		this.title = "";
		this.number = 0;
	}

	/**
	 * Non-Default Constructor
	 */
	public Track(String title, int number)
	{
		this.title = title;
		this.number = number;
	}

	/**
	 * @return the title
	 */
	public String getTitle()
	{
		return title;
	}

	/**
	 * @param title the title to set
	 */
	public void setTitle(String title)
	{
		this.title = title;
	}

	/**
	 * @return the number
	 */
	public int getNumber()
	{
		return number;
	}

	/**
	 * @param number the number to set
	 */
	public void setNumber(int number)
	{
		this.number = number;
	}
}
