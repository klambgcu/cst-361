/**
 * 
 */
package util;

/**
 * ---------------------------------------------------------------
 * Name      : Kelly Lamb
 * Date      : 2022-02-14
 * Class     : CST-361 Design Patterns with Java
 * Professor : Mohamed Mneimneh
 * Assignment: Activity 4
 * Disclaimer: This is our own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Class defining a custom runtime exception to state that a 
 * 2. music album track is not found.
 * 3.
 * ---------------------------------------------------------------
 */
public class TracksNotFoundException extends RuntimeException
{
	/**
	 * A generated ID to identify this custom runtime exception
	 */
	private static final long serialVersionUID = 8749912411505566655L;

	public TracksNotFoundException(String message)
	{
		super(message);
	}
}
