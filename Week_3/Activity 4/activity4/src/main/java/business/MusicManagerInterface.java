package business;

import beans.Album;

/**
 * ---------------------------------------------------------------
 * Name      : Kelly Lamb
 * Date      : 2022-02-14
 * Class     : CST-361 Design Patterns with Java
 * Professor : Mohamed Mneimneh
 * Assignment: Activity 4
 * Disclaimer: This is our own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Interface to manage music albums track information
 * 2.
 * 3.
 * ---------------------------------------------------------------
 */

public interface MusicManagerInterface
{
	Album addAlbum(Album model);
}
