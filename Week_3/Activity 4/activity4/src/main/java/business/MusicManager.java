package business;

import beans.Album;
import beans.Track;
import util.TracksNotFoundException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * ---------------------------------------------------------------
 * Name      : Kelly Lamb
 * Date      : 2022-02-14
 * Class     : CST-361 Design Patterns with Java
 * Professor : Mohamed Mneimneh
 * Assignment: Activity 4
 * Disclaimer: This is our own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Concrete class to manage music albums track information
 * 2.
 * 3.
 * ---------------------------------------------------------------
 */

public class MusicManager implements MusicManagerInterface
{
	// Combination of Artist + Album Title + Year that stores a List of Tracks
	private HashMap<String, List<Track>> TrackInfo;
	
	/**
     * Default constructor. 
     */
    public MusicManager()
    {
        TrackInfo = new HashMap<String, List<Track>>();

        // Add Journey Escape Album
        String key = "JourneyEscape1981";
        List<Track> list = new ArrayList<Track>();
        list.add(new Track("Don't Stop Believin'", 1));
        list.add(new Track("Stone in Love", 2));
        list.add(new Track("Who's Crying Now",3));
        list.add(new Track("Keep On Runnin'", 4));
        list.add(new Track("Still They Ride",5));
        list.add(new Track("Escape", 6));
        list.add(new Track("Lay It Down", 7));
        list.add(new Track("Dead or Alive", 8));
        list.add(new Track("Mother, Father", 9));
        list.add(new Track("Open Arms", 10));        
        TrackInfo.put(key, list);
        
        // Add The Cure Wishes Album
        key = "The CureWishes1992";
        list = new ArrayList<Track>();
        list.add(new Track("Open", 1));
        list.add(new Track("High", 2));
        list.add(new Track("Apart", 3));
        list.add(new Track("From the Edge of the Deep Green Sea", 4));
        list.add(new Track("Wendy Time", 5));
        list.add(new Track("Doing the Unstuck", 6));
        list.add(new Track("Friday I'm in Love", 7));
        list.add(new Track("Trust", 8));
        list.add(new Track("A Letter to Elise", 9));
        list.add(new Track("Cut", 10));
        list.add(new Track("To Wish Impossible Things", 11));
        list.add(new Track("End", 12));
        TrackInfo.put(key, list);
    }

	/**
     * @see MusicManagerInterface#addAlbum(Album)
     */
    public Album addAlbum(Album model) throws TracksNotFoundException
    {
    	List<Track> list = getTracks(model);
    	if (null == list) throw new TracksNotFoundException("Album Tracks Not Defined.");
    	model.setTracks(list);
    	return model;
    }

	private List<Track> getTracks(Album album)
	{
		String key = album.getArtist().trim() + album.getTitle().trim() + Integer.toString(album.getYear()).trim();
		return TrackInfo.get(key);
	}
}
