package controllers;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

import beans.Album;
import business.MusicManager;
import business.MusicManagerInterface;
import util.TracksNotFoundException;

/**
 * ---------------------------------------------------------------
 * Name      : Kelly Lamb
 * Date      : 2022-02-114
 * Class     : CST-361 Design Patterns with Java
 * Professor : Mohamed Mneimneh
 * Assignment: Activity 4
 * Disclaimer: This is our own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Class defining a controller for album management
 * 2.
 * 3.
 * ---------------------------------------------------------------
 */
@ManagedBean
@ViewScoped
public class AlbumController
{	
	public String onSubmit(Album album) 
	{
		MusicManagerInterface mm = new MusicManager();
		
		try
		{
			album = mm.addAlbum(album);
		}
		catch(TracksNotFoundException e)
		{
			System.out.println(e.getMessage());
		}
		
		FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("album", album);
        
        return "AddAlbumResponse.xhtml";
	}
}
