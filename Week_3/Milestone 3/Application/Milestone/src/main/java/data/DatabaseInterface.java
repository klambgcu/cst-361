package data;

import java.util.List;

import beans.Role;
import beans.User;

/**
 * ---------------------------------------------------------------
 * Name      : Group 1 (Kelly Lamb, Brian Cantrell)
 * Date      : 2022-02-14
 * Class     : CST-361 Design Patterns with Java
 * Professor : Mohamed Mneimneh
 * Assignment: Milestone (Motion Devices)
 * Disclaimer: This is our own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Interface to house all database CRUD exposed methods
 * 2. 
 * 3.
 * ---------------------------------------------------------------
 */

public interface DatabaseInterface
{
    //-----------------------------------------------------
    // All exposed methods for the User DAO
    //-----------------------------------------------------
    
    /**
     * 
     * @return List : All users from the database
     */
    public List<User> readAllUsers();
    
    /**
     * 
     * @param user : User to insert into the database
     * @return boolean: true(successful), false otherwise
     */
    public boolean createUser(User user);
    
    /**
     * 
     * @param id: Integer ID (Key) to select user
     * @return User: User object if found otherwise null
     */
    public User selectUserById(int id);
    
    /**
     * 
     * @param user : User model to delete from database (ID)
     * @return boolean: true(successful), false otherwise
     */
    public boolean deleteUser(User user);
    
    /**
     * 
     * @param user : User object to update
     * @return boolean: true(successful), false otherwise
     */
    public boolean updateUser(User user);

    /**
     * 
     * @param email : String name to search for user
     * @param password : String name to search for user
     * @return User: User object if found otherwise null
     */
    public User selectUserByEmailPassword(String email, String password);
    
    /**
     * 
     * @param email : email should be unique - verify
     * @return boolean: true(exists), false otherwise
     */
    public boolean doesEmailExist(String email);
    
    //-----------------------------------------------------
    // All exposed methods for the Role DAO
    //-----------------------------------------------------
    /**
     * 
     * @return List : All roles from the database
     */
    public List<Role> readAllRoles();
    
    /**
     * 
     * @param role : Role to insert into the database
     * @return boolean: true(successful), false otherwise
     */
    public boolean createRole(Role role);
    
    /**
     * 
     * @param id: Integer ID (Key) to select role
     * @return Role: User object if found otherwise null
     */
    public Role selectRoleById(int id);
    
    /**
     * 
     * @param role : Role model to delete from database (ID)
     * @return boolean: true(successful), false otherwise
     */
    public boolean deleteRole(Role role);
    
    /**
     * 
     * @param role : Role object to update
     * @return boolean: true(successful), false otherwise
     */
    public boolean updateRole(Role role);
    
}
