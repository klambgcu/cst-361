package data;

import java.util.List;

import beans.Role;
import beans.User;

/**
 * ---------------------------------------------------------------
 * Name      : Group 1 (Kelly Lamb, Brian Cantrell)
 * Date      : 2022-02-14
 * Class     : CST-361 Design Patterns with Java
 * Professor : Mohamed Mneimneh
 * Assignment: Milestone (Motion Devices)
 * Disclaimer: This is our own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Service to handle all database CRUD for exposed methods
 * 2. 
 * 3.
 * ---------------------------------------------------------------
 */

public class DatabaseService implements DatabaseInterface
{
    private UserDAO userDAO = new UserDAO();
    private RoleDAO roleDAO = new RoleDAO();

	/**
	 * Default Constructor
	 */
	public DatabaseService() {}

    /**
     * 
     * @return List : All users from the database
     */
	@Override
    public List<User> readAllUsers()
	{
        return userDAO.findAll();
    }

    /**
     * 
     * @param user : User to insert into the database
     * @return boolean: true(successful), false otherwise
     */
	@Override
    public boolean createUser(User user)
	{
        return userDAO.create(user);
    }
    
	   /**
     * 
     * @param id: Integer ID (Key) to select user
     * @return User: User object if found otherwise null
     */
	@Override
    public User selectUserById(int id)
	{
        return userDAO.findById(id);
    }
    
    /**
     * 
     * @param user : User model to delete from database (ID)
     * @return boolean: true(successful), false otherwise
     */
	@Override
    public boolean deleteUser(User user)
	{
        return userDAO.delete(user);
    }
    
    /**
     * 
     * @param user : User object to update
     * @return boolean: true(successful), false otherwise
     */
	@Override
	public boolean updateUser(User user)
	{
        return userDAO.update(user);
    }
    
    /**
     * 
     * @param email : String name to search for user
     * @param password : String name to search for user
     * @return User: User object if found otherwise null
     */
	@Override
    public User selectUserByEmailPassword(String email, String password)
    {
		return userDAO.getByEmailPassword(email, password);
    }
    
    /**
     * 
     * @param email : email should be unique - verify
     * @return boolean: true(exists), false otherwise
     */
	@Override
    public boolean doesEmailExist(String email)
    {
		return userDAO.emailExists(email);
    }
    
	
    //-----------------------------------------------------
    // All exposed methods for the Role DAO
    //-----------------------------------------------------
	
    /**
     * 
     * @return List : All roles from the database
     */
	@Override
	public List<Role> readAllRoles()
	{
		return roleDAO.findAll();
	}

    /**
     * 
     * @param role : Role to insert into the database
     * @return boolean: true(successful), false otherwise
     */
	@Override
	public boolean createRole(Role role)
	{
		return roleDAO.create(role);
	}

    /**
     * 
     * @param id: Integer ID (Key) to select role
     * @return Role: User object if found otherwise null
     */
	@Override
	public Role selectRoleById(int id)
	{
		return roleDAO.findById(id);
	}

    /**
     * 
     * @param role : Role model to delete from database (ID)
     * @return boolean: true(successful), false otherwise
     */
	@Override
	public boolean deleteRole(Role role)
	{
		return roleDAO.delete(role);
	}

    /**
     * 
     * @param role : Role object to update
     * @return boolean: true(successful), false otherwise
     */
	@Override
	public boolean updateRole(Role role)
	{
		return roleDAO.update(role);
	}
}
