package business;

import java.util.List;

import beans.User;
import data.DatabaseInterface;
import data.DatabaseService;

/**
 * ---------------------------------------------------------------
 * Name      : Group 1 (Kelly Lamb, Brian Cantrell)
 * Date      : 2022-02-14
 * Class     : CST-361 Design Patterns with Java
 * Professor : Mohamed Mneimneh
 * Assignment: Milestone (Motion Devices)
 * Disclaimer: This is our own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Class to handle all business processes for users
 * 2. 
 * 3.
 * ---------------------------------------------------------------
 */

public class UserBusinessService implements UserBusinessInterface
{
    private final DatabaseInterface db = new DatabaseService();

	/**
	 * Default Constructor
	 */
	public UserBusinessService() {}

	@Override
	public List<User> readAllUsers()
	{
		return db.readAllUsers();
	}

	@Override
	public boolean createUser(User user)
	{
		return db.createUser(user);
	}

	@Override
	public User selectUserById(int id)
	{
		return db.selectUserById(id);
	}

	@Override
	public boolean deleteUser(User user)
	{
		return db.deleteUser(user);
	}

	@Override
	public boolean updateUser(User user)
	{
		return db.updateUser(user);
	}

	@Override
	public User selectUserByEmailPassword(String email, String password)
	{
		return db.selectUserByEmailPassword(email, password);
	}

	@Override
	public boolean doesEmailExist(String email)
	{
		return db.doesEmailExist(email);
	}
}
