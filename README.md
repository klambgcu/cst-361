# README #

Motion Detection System (Milestone) along with individual assignments as necessary

### What is this repository for? ###

To hold individual assignments and group assignment (Milestone)
Group Members: (Kelly Lamb, Brian Cantrell)

### How do I get set up? ###

* Summary of set up
Oracle JDK 1.8
Eclipse (EE/Web) 2021-12
JBoss WildFly 13.0.0

* Configuration
To Do

* Dependencies
To Do

* Database configuration
MySql
To Do DDL Script

* How to run tests
Local: http://localhost:8080/

* Deployment instructions
To Do

### Contribution guidelines ###

To Do: Multiple Assignments Include CLC - Group Assignment

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

Kelly Lamb: klamb13@my.gcu.edu

