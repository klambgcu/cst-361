-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Feb 06, 2022 at 07:25 PM
-- Server version: 5.7.24
-- PHP Version: 8.0.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cst-361`
--
CREATE DATABASE IF NOT EXISTS `cst-361` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `cst-361`;

-- --------------------------------------------------------

--
-- Table structure for table `devices`
--
-- Creation: Feb 06, 2022 at 07:17 PM
--

DROP TABLE IF EXISTS `devices`;
CREATE TABLE IF NOT EXISTS `devices` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `CODE` int(11) NOT NULL,
  `NAME` varchar(100) NOT NULL,
  `USER_ID` int(11) NOT NULL,
  `ACTIVATED` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `USER_ID_IDX2` (`USER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Truncate table before insert `devices`
--

TRUNCATE TABLE `devices`;
-- --------------------------------------------------------

--
-- Table structure for table `motions`
--
-- Creation: Feb 06, 2022 at 07:22 PM
--

DROP TABLE IF EXISTS `motions`;
CREATE TABLE IF NOT EXISTS `motions` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `DEVICE_ID` int(11) NOT NULL,
  `TRIGGERED_TIMESTAMP` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`),
  KEY `DEVICE_ID_IDX1` (`DEVICE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Truncate table before insert `motions`
--

TRUNCATE TABLE `motions`;
-- --------------------------------------------------------

--
-- Table structure for table `users`
--
-- Creation: Feb 06, 2022 at 07:13 PM
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `FIRSTNAME` int(50) NOT NULL,
  `LASTNAME` int(50) NOT NULL,
  `USERNAME` int(100) NOT NULL,
  `EMAIL` varchar(100) NOT NULL,
  `PASSWORD` varchar(250) NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `EMAIL_UNIQ_IDX1` (`EMAIL`),
  UNIQUE KEY `USERNAME_UNIQ_IDX1` (`USERNAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Truncate table before insert `users`
--

TRUNCATE TABLE `users`;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
