package activity2a;

/**
 * @author Kelly Lamb
 *
 */
public abstract class Observer
{
	protected Subject subject;
	public abstract void update();
}
