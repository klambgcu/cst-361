package activity2b;

/**
 * @author Kelly Lamb
 *
 */
public interface Shape
{
	void draw();
}
