package business;

import beans.Album;
import util.AlbumNotFoundException;
import util.TracksNotFoundException;

/**
 * ---------------------------------------------------------------
 * Name      : Kelly Lamb
 * Date      : 2022-02-21
 * Class     : CST-361 Design Patterns with Java
 * Professor : Mohamed Mneimneh
 * Assignment: Activity 5
 * Disclaimer: This is our own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Interface to manage music albums track information
 * 2.
 * 3.
 * ---------------------------------------------------------------
 */

public interface MusicManagerInterface
{
	Album addAlbum(Album model) throws TracksNotFoundException;
	Album getAlbum(Album model) throws AlbumNotFoundException;
}
