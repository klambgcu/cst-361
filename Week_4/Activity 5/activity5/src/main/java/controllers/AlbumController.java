package controllers;


import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import beans.Album;
import business.MusicManager;
import business.MusicManagerInterface;
import util.AlbumNotFoundException;
import util.TracksNotFoundException;

/**
 * ---------------------------------------------------------------
 * Name      : Kelly Lamb
 * Date      : 2022-02-114
 * Class     : CST-361 Design Patterns with Java
 * Professor : Mohamed Mneimneh
 * Assignment: Activity 4
 * Disclaimer: This is our own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Class defining a controller for album management
 * 2.
 * 3.
 * ---------------------------------------------------------------
 */
@ManagedBean
@ViewScoped
public class AlbumController
{	
	MusicManagerInterface manager;
	
	public AlbumController()
	{
		manager = new MusicManager();
	}
	
	public String onSubmit(Album album) 
	{
		
		try
		{
			album = manager.addAlbum(album);
		}
		catch(TracksNotFoundException e)
		{
			System.out.println(e.getMessage());
			
			// Add View Faces Message
			FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, e.getMessage(), e.getMessage());
			// The component id is null, so this message is considered as a view message
			FacesContext.getCurrentInstance().addMessage(null, message);
			// Return empty token for navigation handler
			return "";
		}
		
		FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("album", album);
        
        return "AddAlbumResponse.xhtml";
	}
	
	public String onFind(Album album) 
	{
		try
		{
			album = manager.getAlbum(album);
		}
		catch(AlbumNotFoundException e)
		{
			System.out.println(e.getMessage());
			
			// Add View Faces Message
			FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, e.getMessage(), e.getMessage());
			// The component id is null, so this message is considered as a view message
			FacesContext.getCurrentInstance().addMessage(null, message);
			// Return empty token for navigation handler
			return "";
		}
		
		FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("album", album);
        
        return "AddAlbumResponse.xhtml";
	}
}
