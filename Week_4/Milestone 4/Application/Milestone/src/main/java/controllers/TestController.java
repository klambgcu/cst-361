package controllers;

import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;

import beans.Device;
import beans.Motion;
import beans.Role;
import beans.User;
import business.DeviceBusinessInterface;
import business.DeviceBusinessService;
import business.MotionBusinessInterface;
import business.MotionBusinessService;
import business.RoleBusinessInterface;
import business.RoleBusinessService;
import business.UserBusinessInterface;
import business.UserBusinessService;
import data.DeviceDAO;
import data.MotionDAO;
import data.RoleDAO;
import data.UserDAO;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

/**
 * ---------------------------------------------------------------
 * Name      : Group 1 (Kelly Lamb, Brian Cantrell)
 * Date      : 2022-02-14
 * Class     : CST-361 Design Patterns with Java
 * Professor : Mohamed Mneimneh
 * Assignment: Milestone (Motion Devices)
 * Disclaimer: This is our own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Temporary Testing Controller
 * 2. 
 * 3.
 * ---------------------------------------------------------------
 */

@ManagedBean
public class TestController
{

	public String testDeviceListing()
	{
		List<Device> devices = new ArrayList<Device>();
		
		for (int x = 1; x <= 100; x++)
			devices.add( new Device(x, x, "Device: " + x, 4, true));
		
		
		FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("devices", devices);
		
		return "devices.xhtml";
	}
	
	// Temporary Test Method
	public void test()
	{
		System.out.println("TestController:test method - enter");

		// testMotionBusiness(); // All Tests Successful
		// testDeviceBusiness(); // All Tests Successful
		// testRoleBusiness();   // All Tests Successful
		// testUserBusiness();   // All Tests Successful

		// testUserDAO();   // All Tests Successful
		// testRoleDAO();   // All Tests Successful
		// testDeviceDAO(); // All Tests Successful
		// testMotionDAO(); // All Tests Successful
		
		System.out.println("TestController:test method - exit");
	}

	private void testMotionBusiness()
	{
		MotionBusinessInterface service = new MotionBusinessService();

		List<Motion> list = service.readAllMotions();
		
		for (Motion item: list)
		{
			System.out.println("" + item.getId() + item.getDevice_id() + item.getTriggered_timestamp());
		}
		
		Motion item = service.selectMotionById(1);
		System.out.println("" + item.getId() + item.getDevice_id() + item.getTriggered_timestamp());

		java.util.Date date = new java.util.Date();
		item = new Motion(0,6, new Timestamp(date.getTime()));
		//service.createMotion(item);

		
		item.setId(30);
		item.setDevice_id(5);
		item.setTriggered_timestamp(new Timestamp(date.getTime()));
		service.updateMotion(item);
				
		service.deleteMotion(item);
		
	}
	
	private void testDeviceBusiness()
	{
		DeviceBusinessInterface service = new DeviceBusinessService();

		List<Device> list = service.readAllDevices();
		
		for (Device item: list)
		{
			System.out.println("" + item.getId() + item.getCode() + item.getName() + item.isActivated());
		}
		
		Device item = service.selectDeviceById(1);
		System.out.println("" + item.getId() + item.getCode() + item.getName() + item.isActivated());

		item = new Device(0, 100, "TESTDEVICE", 4, true);
		//service.createDevice(item);

		item.setId(9);
		item.setName("TESTDEVICEUPDATE");
		item.setCode(500);
		service.updateDevice(item);
				
		service.deleteDevice(item);
		
		
	}

	
	private void testRoleBusiness()
	{
		RoleBusinessInterface service = new RoleBusinessService();

		List<Role> list = service.readAllRoles();
		
		for (Role item: list)
		{
			System.out.println(item.getId() + item.getRolename() + item.getDescription());
		}
		
		Role item = service.selectRoleById(1);
		System.out.println(item.getId() + item.getRolename() + item.getDescription());

		item = new Role(0, "TESTROLENAME", "TESTROLEDESCRIPTION");
		// service.createRole(item);

		item.setId(6);
		item.setRolename("TESTROLENAMEUPDATE");
		item.setDescription("TESTROLEDESCRIPTIONUPDATE");
		service.updateRole(item);
				
		service.deleteRole(item);
	}
	
	
	private void testUserBusiness()
	{
		UserBusinessInterface service = new UserBusinessService();

		List<User> list = service.readAllUsers();
		
		for (User item: list)
		{
			System.out.println(item.getId() + item.getFirstname() + item.getLastname() + item.getEmail() + item.getMobile()+item.getPassword()+item.getRole_id());
		}

		
		User item = service.selectUserById(1);
		System.out.println(item.getId() + item.getFirstname() + item.getLastname() + item.getEmail() + item.getMobile()+item.getPassword()+item.getRole_id());

		if (service.doesEmailExist("kl@kl.com"))
			System.out.println("kl@kl.com email exists");
		
		if (! service.doesEmailExist("kl123132@kl.com"))
			System.out.println("kl123132@kl.com email does not exists");

		//item = new User(0, "TESTUSERFIRST", "TESTUSERLAST", "EMAIL@EMAIL.COM", "(999) 999-9999", "PASSWORD", 1);	
		//service.createUser(item);
		
		item = new User(8, "TESTUSERFIRSTUPDATE", "TESTUSERLASTUPDATE", "EMAIL@EMAIL.COMUPDATE", "(999) 999-9999UPDATE", "PASSWORDUPDATE", 2);
		service.updateUser(item);
		
		User item2 = service.selectUserByEmailPassword("EMAIL@EMAIL.COMUPDATE", "PASSWORDUPDATE");
		System.out.println(item2.getId() + item2.getFirstname() + item2.getLastname() + item2.getEmail() + item2.getMobile()+item2.getPassword()+item2.getRole_id());
		
		if (service.deleteUser(item2))
			System.out.println("User 8 deleted");
	}
	
	
	
	// Temporary Test Method
	private void testRoleDAO()
	{
		RoleDAO dao = new RoleDAO();
		
		List<Role> list = dao.findAll();
		
		for (Role item: list)
		{
			System.out.println(item.getId() + item.getRolename() + item.getDescription());
		}
		
		Role item = dao.findById(1);
		System.out.println(item.getId() + item.getRolename() + item.getDescription());

		item = new Role(0, "TESTROLENAME", "TESTROLEDESCRIPTION");
		// dao.create(item);
		item.setId(5);
		item.setRolename("TESTROLENAMEUPDATE");
		item.setDescription("TESTROLEDESCRIPTIONUPDATE");
		dao.update(item);
		
		Role item1 = dao.findBy(item);
		System.out.println(item1.getId() + item1.getRolename() + item1.getDescription());
		
		dao.delete(item);
	}
		
	// Temporary Test Method
	private void testUserDAO()
	{		
		UserDAO dao = new UserDAO();
		
		List<User> list = dao.findAll();
		
		for (User item: list)
		{
			System.out.println(item.getId() + item.getFirstname() + item.getLastname() + item.getEmail() + item.getMobile()+item.getPassword()+item.getRole_id());
		}
				
		User item = dao.findById(1);
		System.out.println(item.getId() + item.getFirstname() + item.getLastname() + item.getEmail() + item.getMobile()+item.getPassword()+item.getRole_id());

		if (dao.emailExists("kl@kl.com"))
			System.out.println("kl@kl.com email exists");
		
		if (! dao.emailExists("kl123132@kl.com"))
			System.out.println("kl123132@kl.com email does not exists");

		item = new User(0, "TESTUSERFIRST", "TESTUSERLAST", "EMAIL@EMAIL.COM", "(999) 999-9999", "PASSWORD", 1);	
		dao.create(item);
		
		item = new User(6, "TESTUSERFIRSTUPDATE", "TESTUSERLASTUPDATE", "EMAIL@EMAIL.COMUPDATE", "(999) 999-9999UPDATE", "PASSWORDUPDATE", 2);
		dao.update(item);
		
		User item2 = dao.getByEmailPassword("EMAIL@EMAIL.COMUPDATE", "PASSWORDUPDATE");
		System.out.println(item2.getId() + item2.getFirstname() + item2.getLastname() + item2.getEmail() + item2.getMobile()+item2.getPassword()+item2.getRole_id());
		
		if (dao.delete(item))
			System.out.println("User 6 deleted");
		
	}
	
	// Temporary Test Method
	private void testDeviceDAO()
	{
		DeviceDAO dao = new DeviceDAO();

		List<Device> list = dao.findAll();
		
		for (Device item: list)
		{
			System.out.println("" + item.getId() + item.getCode() + item.getName() + item.getUser_id() + item.isActivated());
		}
				
		Device item = dao.findById(1);
		System.out.println("" + item.getId() + item.getCode() + item.getName() + item.getUser_id() + item.isActivated());

		item = new Device(0, 1, "TESTDEVICE", 4, true);
		//dao.create(item);
		
		item = new Device(7, 10, "TESTDEVICEUPDATE", 4, false);
		dao.update(item);
		
		item = new Device(8, 10, "TESTDEVICEUPDATE", 4, false);
		dao.delete(item);
	}
	
	// Temporary Test Method
	private void testMotionDAO()
	{
		MotionDAO dao = new MotionDAO();

		List<Motion> list = dao.findAll();
		
		for (Motion item: list)
		{
			System.out.println("" + item.getId() + item.getDevice_id() + item.getTriggered_timestamp());
		}
				
		Motion item = dao.findById(1);
		System.out.println("" + item.getId() + item.getDevice_id() + item.getTriggered_timestamp());

		//item = new Motion(0,1, new Timestamp(date.getTime()));
		//dao.create(item);
		
		java.util.Date date = new java.util.Date();
		item = new Motion(28,2, new Timestamp( date.getTime() ));
		dao.update(item);
		
		dao.delete(item);
	}


}
