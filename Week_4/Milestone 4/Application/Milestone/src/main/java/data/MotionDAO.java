package data;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import beans.Motion;
import util.DatabaseException;

/**
 * ---------------------------------------------------------------
 * Name      : Group 1 (Kelly Lamb, Brian Cantrell)
 * Date      : 2022-02-14
 * Class     : CST-361 Design Patterns with Java
 * Professor : Mohamed Mneimneh
 * Assignment: Milestone (Motion Devices)
 * Disclaimer: This is our own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Class defining a DAO for Motion CRUD
 * 2. 
 * 3.
 * ---------------------------------------------------------------
 */

public class MotionDAO extends DatabaseConfig implements DataAccessInterface<Motion>
{
	//
	// Define query strings
	//
    private final static String SQL_SELECT_ALL_MOTION   = "select * from motions order by id";
    private final static String SQL_SELECT_MOTION_BY_ID = "select * from motions where id = ?";
    private final static String SQL_INSERT_MOTION       = "insert into motions (device_id) values (?)";
    private final static String SQL_DELETE_MOTION_BY_ID = "delete from motions where id = ?";
    private final static String SQL_UPDATE_MOTION       = "update motions set device_id = ?, triggered_timestamp = ? where id = ?";

	/**
	 * Default Constructor
	 */
	public MotionDAO() {}

	/**
	 * Return all Motions in the database
	 * @return List<Motion> A list containing all the Motions 
	 */
	@Override
	public List<Motion> findAll()
	{
        List<Motion> Motions = new ArrayList<>();

        try (Connection connection = getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SQL_SELECT_ALL_MOTION);)
        {
            ResultSet rs = preparedStatement.executeQuery();

            while (rs.next())
            {
                int id                         = rs.getInt("id");
                int device_id                  = rs.getInt("device_id");
                Timestamp triggered_timestamp  = rs.getTimestamp("triggered_timestamp");
                Motions.add(new Motion(id, device_id, triggered_timestamp));
            }
        }
        catch (SQLException e)
        {
			e.printStackTrace();
			throw new DatabaseException(e);
        }
        catch (Exception e)
        {
			e.printStackTrace();
			throw new DatabaseException(e);
        }
        
        return Motions;
	}

	/**
	 * @param id integer key
	 * @return Motion - object containing Motion information
	 */
	@Override
	public Motion findById(int id)
	{
		Motion Motion = null;

        try (Connection connection = getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SQL_SELECT_MOTION_BY_ID);)
        {
            preparedStatement.setInt(1, id);

            ResultSet rs = preparedStatement.executeQuery();

            while (rs.next())
            {
                int device_id                  = rs.getInt("device_id");
                Timestamp triggered_timestamp  = rs.getTimestamp("triggered_timestamp");
                Motion = new Motion(id, device_id, triggered_timestamp);
            }
        }
        catch (SQLException e)
        {
			e.printStackTrace();
			throw new DatabaseException(e);
        }
        catch (Exception e)
        {
			e.printStackTrace();
			throw new DatabaseException(e);
        }
            
        return Motion;
	}

	/**
	 * Find a Motion by Motion model - uses ID only
	 * @param t Motion model
	 * @return Motion model as determined by ID in model (same as findById)
	 */
	@Override
	public Motion findBy(Motion t)
	{
		Motion Motion = null;

        try (Connection connection = getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SQL_SELECT_MOTION_BY_ID);)
        {
            preparedStatement.setInt(1, t.getId());

            ResultSet rs = preparedStatement.executeQuery();

            while (rs.next())
            {
                int device_id                  = rs.getInt("device_id");
                Timestamp triggered_timestamp  = rs.getTimestamp("triggered_timestamp");
                Motion = new Motion(t.getId(), device_id, triggered_timestamp);
            }
        }
        catch (SQLException e)
        {
			e.printStackTrace();
			throw new DatabaseException(e);
        }
        catch (Exception e)
        {
			e.printStackTrace();
			throw new DatabaseException(e);
        }
            
        return Motion;
	}

	/**
	 * Create a new Motion
	 * @param t Motion model populated - no id needed (automatic generation)
	 * @return boolean created(true), not created(false)
	 */
	@Override
	public boolean create(Motion t)
	{
        int rowsAffected = 0;

        try (Connection connection = getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SQL_INSERT_MOTION, Statement.RETURN_GENERATED_KEYS))
        {
            preparedStatement.setInt(1, t.getDevice_id());

            rowsAffected = preparedStatement.executeUpdate();
            
            if (rowsAffected == 0)
            {
                throw new SQLException("Creating Motion failed, no rows affected.");
            }
        }
        catch (SQLException e)
        {
			e.printStackTrace();
			throw new DatabaseException(e);
        }
        catch (Exception e)
        {
			e.printStackTrace();
			throw new DatabaseException(e);
        }
        
        return (rowsAffected > 0);
	}

	/**
	 * Update the Motion information based upon model
	 * @param t Motion model
	 * @return boolean updated(true), not updated(false)
	 */
	@Override
	public boolean update(Motion t)
	{
        int rowsAffected = 0;
        
        try (Connection connection = getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SQL_UPDATE_MOTION);)
        {
            preparedStatement.setInt(1, t.getDevice_id());
            preparedStatement.setTimestamp(2, t.getTriggered_timestamp());
            preparedStatement.setInt(3, t.getId());

            rowsAffected = preparedStatement.executeUpdate();
        }
        catch (SQLException e)
        {
			e.printStackTrace();
			throw new DatabaseException(e);
        }
        catch (Exception e)
        {
			e.printStackTrace();
			throw new DatabaseException(e);
        }

        return (rowsAffected > 0);
	}

	/**
	 * Delete a Motion
	 * @param t Motion to delete - uses id from model
	 * @return boolean delete(true), no delete(false)
	 */
	@Override
	public boolean delete(Motion t)
	{
        int rowsAffected = 0;

        try (Connection connection = getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SQL_DELETE_MOTION_BY_ID);)
        {
            preparedStatement.setInt(1, t.getId());

            rowsAffected = preparedStatement.executeUpdate();
        }
        catch (SQLException e)
        {
			e.printStackTrace();
			throw new DatabaseException(e);
        }
        catch (Exception e)
        {
			e.printStackTrace();
			throw new DatabaseException(e);
        }
        
        return (rowsAffected > 0);
	}
}
