package data;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import beans.Role;
import util.DatabaseException;

/**
 * ---------------------------------------------------------------
 * Name      : Group 1 (Kelly Lamb, Brian Cantrell)
 * Date      : 2022-02-14
 * Class     : CST-361 Design Patterns with Java
 * Professor : Mohamed Mneimneh
 * Assignment: Milestone (Motion Devices)
 * Disclaimer: This is our own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Class defining a DAO for Roles CRUD
 * 2. 
 * 3.
 * ---------------------------------------------------------------
 */

public class RoleDAO extends DatabaseConfig implements DataAccessInterface<Role>
{
	//
	// Define query strings
	//
    private final static String SQL_SELECT_ALL_ROLE   = "select * from roles order by id";
    private final static String SQL_SELECT_ROLE_BY_ID = "select * from roles where id = ?";
    private final static String SQL_INSERT_ROLE       = "insert into roles (rolename, description) values (?, ?)";
    private final static String SQL_DELETE_ROLE_BY_ID = "delete from roles where id = ?";
    private final static String SQL_UPDATE_ROLE       = "update roles set rolename = ?, description = ? where id = ?";

	/**
	 * Default Constructor
	 */
	public RoleDAO() {}

	/**
	 * Return all roles in the database
	 * @return List<Role> A list containing all the roles 
	 */
	@Override
	public List<Role> findAll()
	{
        List<Role> roles = new ArrayList<>();

        try (Connection connection = getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SQL_SELECT_ALL_ROLE);)
        {
            ResultSet rs = preparedStatement.executeQuery();

            while (rs.next())
            {
                int id              = rs.getInt("id");
                String rolename     = rs.getString("rolename");
                String description  = rs.getString("description");
                roles.add(new Role(id, rolename, description));
            }
        }
        catch (SQLException e)
        {
			e.printStackTrace();
			throw new DatabaseException(e);
        }
        catch (Exception e)
        {
			e.printStackTrace();
			throw new DatabaseException(e);
        }
        
        return roles;
	}

	/**
	 * @param id integer key
	 * @return Role - object containing role information
	 */
	@Override
	public Role findById(int id)
	{
		Role role = null;

        try (Connection connection = getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SQL_SELECT_ROLE_BY_ID);)
        {
            preparedStatement.setInt(1, id);

            ResultSet rs = preparedStatement.executeQuery();

            while (rs.next())
            {
                String rolename     = rs.getString("rolename");
                String description  = rs.getString("description");
                role = new Role(id, rolename, description);
            }
        }
        catch (SQLException e)
        {
			e.printStackTrace();
			throw new DatabaseException(e);
        }
        catch (Exception e)
        {
			e.printStackTrace();
			throw new DatabaseException(e);
        }
            
        return role;
	}

	/**
	 * Find a role by role model - uses ID only
	 * @param t Role model
	 * @return Role model as determined by ID in model (same as findById)
	 */
	@Override
	public Role findBy(Role t)
	{
		Role role = null;

        try (Connection connection = getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SQL_SELECT_ROLE_BY_ID);)
        {
            preparedStatement.setInt(1, t.getId());

            ResultSet rs = preparedStatement.executeQuery();

            while (rs.next())
            {
                String rolename     = rs.getString("rolename");
                String description  = rs.getString("description");
                role = new Role(t.getId(), rolename, description);
            }
        }
        catch (SQLException e)
        {
			e.printStackTrace();
			throw new DatabaseException(e);
        }
        catch (Exception e)
        {
			e.printStackTrace();
			throw new DatabaseException(e);
        }
            
        return role;
	}

	/**
	 * Create a new role
	 * @param t Role model populated - no id needed (automatic generation)
	 * @return boolean created(true), not created(false)
	 */
	@Override
	public boolean create(Role t)
	{
        int rowsAffected = 0;

        try (Connection connection = getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SQL_INSERT_ROLE, Statement.RETURN_GENERATED_KEYS))
        {
            preparedStatement.setString(1, t.getRolename());
            preparedStatement.setString(2, t.getDescription());

            rowsAffected = preparedStatement.executeUpdate();
            
            if (rowsAffected == 0)
            {
                throw new SQLException("Creating role failed, no rows affected.");
            }
            
        }
        catch (SQLException e)
        {
			e.printStackTrace();
			throw new DatabaseException(e);
        }
        catch (Exception e)
        {
			e.printStackTrace();
			throw new DatabaseException(e);
        }
        
        return (rowsAffected > 0);
	}

	/**
	 * Update the role information based upon model
	 * @param t Role model
	 * @return boolean updated(true), not updated(false)
	 */
	@Override
	public boolean update(Role t)
	{
        int rowsAffected = 0;
        
        try (Connection connection = getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SQL_UPDATE_ROLE);)
        {
            preparedStatement.setString(1, t.getRolename());
            preparedStatement.setString(2, t.getDescription());
            preparedStatement.setInt(3, t.getId());

            rowsAffected = preparedStatement.executeUpdate();
        }
        catch (SQLException e)
        {
			e.printStackTrace();
			throw new DatabaseException(e);
        }
        catch (Exception e)
        {
			e.printStackTrace();
			throw new DatabaseException(e);
        }

        return (rowsAffected > 0);
	}

	/**
	 * Delete a role
	 * @param t Role to delete - uses id from model
	 * @return boolean delete(true), no delete(false)
	 */
	@Override
	public boolean delete(Role t)
	{
        int rowsAffected = 0;

        try (Connection connection = getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SQL_DELETE_ROLE_BY_ID);)
        {
            preparedStatement.setInt(1, t.getId());

            rowsAffected = preparedStatement.executeUpdate();
        }
        catch (SQLException e)
        {
			e.printStackTrace();
			throw new DatabaseException(e);
        }
        catch (Exception e)
        {
			e.printStackTrace();
			throw new DatabaseException(e);
        }
        
        return (rowsAffected > 0);
	}
}
