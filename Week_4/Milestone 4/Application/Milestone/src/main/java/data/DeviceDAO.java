package data;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import beans.Device;
import util.DatabaseException;

/**
 * ---------------------------------------------------------------
 * Name      : Group 1 (Kelly Lamb, Brian Cantrell)
 * Date      : 2022-02-14
 * Class     : CST-361 Design Patterns with Java
 * Professor : Mohamed Mneimneh
 * Assignment: Milestone (Motion Devices)
 * Disclaimer: This is our own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Class defining a DAO for Devices CRUD
 * 2. 
 * 3.
 * ---------------------------------------------------------------
 */

public class DeviceDAO extends DatabaseConfig implements DataAccessInterface<Device>
{
	//
	// Define query strings
	//
    private final static String SQL_SELECT_ALL_DEVICE   = "select * from devices order by id";
    private final static String SQL_SELECT_DEVICE_BY_ID = "select * from devices where id = ?";
    private final static String SQL_INSERT_DEVICE       = "insert into devices (code, name, user_id, activated) values (?, ?, ?, ?)";
    private final static String SQL_DELETE_DEVICE_BY_ID = "delete from devices where id = ?";
    private final static String SQL_UPDATE_DEVICE       = "update devices set code = ?, name = ?, user_id = ?, activated = ? where id = ?";

	/**
	 * Default Constructor
	 */
	public DeviceDAO() {}

	/**
	 * Return all devices in the database
	 * @return List<Device> A list containing all the devices 
	 */
	@Override
	public List<Device> findAll()
	{
        List<Device> devices = new ArrayList<>();

        try (Connection connection = getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SQL_SELECT_ALL_DEVICE);)
        {
            ResultSet rs = preparedStatement.executeQuery();

            while (rs.next())
            {
                int id            = rs.getInt("id");
                int code          = rs.getInt("code");
                String name       = rs.getString("name");
                int user_id       = rs.getInt("user_id");
                boolean activated = rs.getBoolean("activated");
                devices.add(new Device(id, code, name, user_id, activated));
            }
        }
        catch (SQLException e)
        {
			e.printStackTrace();
			throw new DatabaseException(e);
        }
        catch (Exception e)
        {
			e.printStackTrace();
			throw new DatabaseException(e);
        }
        
        return devices;
	}

	/**
	 * @param id integer key
	 * @return Device - object containing device information
	 */
	@Override
	public Device findById(int id)
	{
		Device device = null;

        try (Connection connection = getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SQL_SELECT_DEVICE_BY_ID);)
        {
            preparedStatement.setInt(1, id);

            ResultSet rs = preparedStatement.executeQuery();

            while (rs.next())
            {
                int code          = rs.getInt("code");
                String name       = rs.getString("name");
                int user_id       = rs.getInt("user_id");
                boolean activated = rs.getBoolean("activated");
                device = new Device(id, code, name, user_id, activated);
            }
        }
        catch (SQLException e)
        {
			e.printStackTrace();
			throw new DatabaseException(e);
        }
        catch (Exception e)
        {
			e.printStackTrace();
			throw new DatabaseException(e);
        }
            
        return device;
	}

	/**
	 * Find a device by device model - uses ID only
	 * @param t Device model
	 * @return Device model as determined by ID in model (same as findById)
	 */
	@Override
	public Device findBy(Device t)
	{
		Device device = null;

        try (Connection connection = getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SQL_SELECT_DEVICE_BY_ID);)
        {
            preparedStatement.setInt(1, t.getId());

            ResultSet rs = preparedStatement.executeQuery();

            while (rs.next())
            {
                int code          = rs.getInt("code");
                String name       = rs.getString("name");
                int user_id       = rs.getInt("user_id");
                boolean activated = rs.getBoolean("activated");
                device = new Device(t.getId(), code, name, user_id, activated);
            }
        }
        catch (SQLException e)
        {
			e.printStackTrace();
			throw new DatabaseException(e);
        }
        catch (Exception e)
        {
			e.printStackTrace();
			throw new DatabaseException(e);
        }
            
        return device;
	}

	/**
	 * Create a new device
	 * @param t Device model populated - no id needed (automatic generation)
	 * @return boolean created(true), not created(false)
	 */
	@Override
	public boolean create(Device t)
	{
        int rowsAffected = 0;

        try (Connection connection = getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SQL_INSERT_DEVICE, Statement.RETURN_GENERATED_KEYS))
        {
            preparedStatement.setInt(1, t.getCode());
            preparedStatement.setString(2, t.getName());
            preparedStatement.setInt(3, t.getUser_id());
            preparedStatement.setBoolean(4, t.isActivated());

            rowsAffected = preparedStatement.executeUpdate();
            
            if (rowsAffected == 0)
            {
                throw new SQLException("Creating device failed, no rows affected.");
            }
        }
        catch (SQLException e)
        {
			e.printStackTrace();
			throw new DatabaseException(e);
        }
        catch (Exception e)
        {
			e.printStackTrace();
			throw new DatabaseException(e);
        }
        
        return (rowsAffected > 0);
	}

	/**
	 * Update the device information based upon model
	 * @param t Device model
	 * @return boolean updated(true), not updated(false)
	 */
	@Override
	public boolean update(Device t)
	{
        int rowsAffected = 0;
        
        try (Connection connection = getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SQL_UPDATE_DEVICE);)
        {
            preparedStatement.setInt(1, t.getCode());
            preparedStatement.setString(2, t.getName());
            preparedStatement.setInt(3, t.getUser_id());
            preparedStatement.setBoolean(4, t.isActivated());
            preparedStatement.setInt(5, t.getId());

            rowsAffected = preparedStatement.executeUpdate();
        }
        catch (SQLException e)
        {
			e.printStackTrace();
			throw new DatabaseException(e);
        }
        catch (Exception e)
        {
			e.printStackTrace();
			throw new DatabaseException(e);
        }

        return (rowsAffected > 0);
	}

	/**
	 * Delete a device
	 * @param t Device to delete - uses id from model
	 * @return boolean delete(true), no delete(false)
	 */
	@Override
	public boolean delete(Device t)
	{
        int rowsAffected = 0;

        try (Connection connection = getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SQL_DELETE_DEVICE_BY_ID);)
        {
            preparedStatement.setInt(1, t.getId());

            rowsAffected = preparedStatement.executeUpdate();
        }
        catch (SQLException e)
        {
			e.printStackTrace();
			throw new DatabaseException(e);
        }
        catch (Exception e)
        {
			e.printStackTrace();
			throw new DatabaseException(e);
        }
        
        return (rowsAffected > 0);
	}
}
