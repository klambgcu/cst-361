package data;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import beans.User;
import util.DatabaseException;

/**
 * ---------------------------------------------------------------
 * Name      : Group 1 (Kelly Lamb, Brian Cantrell)
 * Date      : 2022-02-14
 * Class     : CST-361 Design Patterns with Java
 * Professor : Mohamed Mneimneh
 * Assignment: Milestone (Motion Devices)
 * Disclaimer: This is our own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Class defining a DAO for Users CRUD
 * 2. 
 * 3.
 * ---------------------------------------------------------------
 */

public class UserDAO extends DatabaseConfig implements UserDAOInterface<User>
{
	//
	// Define query strings
	//
    private final static String SQL_SELECT_ALL_USER = "select * from users order by id";
    private final static String SQL_SELECT_USER_BY_ID = "select * from users where id = ?";
    private final static String SQL_INSERT_USER = "insert into users (firstname, lastname, email, mobile, password, role_id) values (?, ?, ?, ?, ?, ?)";
    private final static String SQL_DELETE_USER_BY_ID = "delete from users where id = ?";
    private final static String SQL_UPDATE_USER = "update users set firstname = ?, lastname = ?, email = ?, mobile = ?, password = ?, role_id = ? where id = ?";
    private final static String SQL_SELECT_USER_BY_EMAIL_PASS = "select * from users where email = ? and password = ?";
    private final static String SQL_SELECT_USER_EMAIL_EXISTS = "select count(1) as amount from users where email = ?";
	
	/**
	 * Default Constructor
	 */
	public UserDAO() {}

	/**
	 * @return List<User> A list containing all the users 
	 */
	@Override
	public List<User> findAll()
	{
        List<User> users = new ArrayList<>();

        try (Connection connection = getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SQL_SELECT_ALL_USER);)
        {
            ResultSet rs = preparedStatement.executeQuery();

            while (rs.next())
            {
                int id           = rs.getInt("id");
                String firstname = rs.getString("firstname");
                String lastname  = rs.getString("lastname");
                String email     = rs.getString("email");
                String mobile    = rs.getString("mobile");
                String password  = rs.getString("password");
                int role_id      = rs.getInt("role_id");
                users.add(new User(id, firstname, lastname, email, mobile, password, role_id));
            }
        }
        catch (SQLException e)
        {
			e.printStackTrace();
			throw new DatabaseException(e);
        }
        catch (Exception e)
        {
			e.printStackTrace();
			throw new DatabaseException(e);
        }
        
        return users;
	}

	/**
	 * @param id integer key
	 * @return User - object containing user information
	 */
	@Override
	public User findById(int id)
	{
        User user = null;

        try (Connection connection = getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SQL_SELECT_USER_BY_ID);)
        {
            preparedStatement.setInt(1, id);

            ResultSet rs = preparedStatement.executeQuery();

            while (rs.next())
            {
                String firstname = rs.getString("firstname");
                String lastname  = rs.getString("lastname");
                String email     = rs.getString("email");
                String mobile    = rs.getString("mobile");
                String password  = rs.getString("password");
                int role_id      = rs.getInt("role_id");
                user = new User(id, firstname, lastname, email, mobile, password, role_id);
            }
        }
        catch (SQLException e)
        {
			e.printStackTrace();
			throw new DatabaseException(e);
        }
        catch (Exception e)
        {
			e.printStackTrace();
			throw new DatabaseException(e);
        }
            
        return user;
	}

	/**
	 * Find a user by user model - uses ID only
	 * @param t User model
	 * @return User model as determined by ID in model (same as findById)
	 */
	@Override
	public User findBy(User t)
	{
        User user = null;

        try (Connection connection = getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SQL_SELECT_USER_BY_ID);)
        {
            preparedStatement.setInt(1, t.getId());

            ResultSet rs = preparedStatement.executeQuery();

            while (rs.next())
            {
            	int id           = rs.getInt("id");
                String firstname = rs.getString("firstname");
                String lastname  = rs.getString("lastname");
                String email     = rs.getString("email");
                String mobile    = rs.getString("mobile");
                String password  = rs.getString("password");
                int role_id      = rs.getInt("role_id");
                user = new User(id, firstname, lastname, email, mobile, password, role_id);
            }
        }
        catch (SQLException e)
        {
			e.printStackTrace();
			throw new DatabaseException(e);
        }
        catch (Exception e)
        {
			e.printStackTrace();
			throw new DatabaseException(e);
        }
            
        return user;
	}

	/**
	 * Create a new user
	 * @param t User model populated - no id needed (automatic generation)
	 * @return boolean created(true), not created(false)
	 */
	@Override
	public boolean create(User t)
	{
        int rowsAffected = 0;

        try (Connection connection = getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SQL_INSERT_USER, Statement.RETURN_GENERATED_KEYS))
        {
            preparedStatement.setString(1, t.getFirstname());
            preparedStatement.setString(2, t.getLastname());
            preparedStatement.setString(3, t.getEmail());
            preparedStatement.setString(4, t.getMobile());
            preparedStatement.setString(5, t.getPassword());
            preparedStatement.setInt(6, t.getRole_id());

            rowsAffected = preparedStatement.executeUpdate();
            
            if (rowsAffected == 0)
            {
                throw new SQLException("Creating user failed, no rows affected.");
            }
            
        }
        catch (SQLException e)
        {
			e.printStackTrace();
			throw new DatabaseException(e);
        }
        catch (Exception e)
        {
			e.printStackTrace();
			throw new DatabaseException(e);
        }
        
        return (rowsAffected > 0);
	}

	/**
	 * Update the user information based upon model
	 * @param t User model
	 * @return boolean updated(true), not updated(false)
	 */
	@Override
	public boolean update(User t)
	{
        int rowsAffected = 0;
        
        try (Connection connection = getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SQL_UPDATE_USER);)
        {
            preparedStatement.setString(1, t.getFirstname());
            preparedStatement.setString(2, t.getLastname());
            preparedStatement.setString(3, t.getEmail());
            preparedStatement.setString(4, t.getMobile());
            preparedStatement.setString(5, t.getPassword());
            preparedStatement.setInt(6, t.getRole_id());
            preparedStatement.setInt(7, t.getId());

            rowsAffected = preparedStatement.executeUpdate();
        }
        catch (SQLException e)
        {
			e.printStackTrace();
			throw new DatabaseException(e);
        }
        catch (Exception e)
        {
			e.printStackTrace();
			throw new DatabaseException(e);
        }

        return (rowsAffected > 0);
	}

	/**
	 * Delete a user
	 * @param t User to delete - uses id from model
	 * @return boolean delete(true), no delete(false)
	 */
	@Override
	public boolean delete(User t)
	{
        int rowsAffected = 0;

        try (Connection connection = getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SQL_DELETE_USER_BY_ID);)
        {
            preparedStatement.setInt(1, t.getId());

            rowsAffected = preparedStatement.executeUpdate();
        }
        catch (SQLException e)
        {
			e.printStackTrace();
			throw new DatabaseException(e);
        }
        catch (Exception e)
        {
			e.printStackTrace();
			throw new DatabaseException(e);
        }
        
        return (rowsAffected > 0);
	}

	/**
	 * Obtain user based upon email and password - null is not valid credentials
	 * @param email String - email to validate
	 * @param password String - password to validate
	 * @return User user info or null
	 */
	@Override
	public User getByEmailPassword(String email, String password)
	{
        User user = null;

        try (Connection connection = getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SQL_SELECT_USER_BY_EMAIL_PASS);)
        {
            preparedStatement.setString(1, email);
            preparedStatement.setString(2, password);

            ResultSet rs = preparedStatement.executeQuery();

            while (rs.next())
            {
            	int id           = rs.getInt("id");
                String firstname = rs.getString("firstname");
                String lastname  = rs.getString("lastname");
                String mobile    = rs.getString("mobile");
                int role_id      = rs.getInt("role_id");
                user = new User(id, firstname, lastname, email, mobile, password, role_id);
            }
        }
        catch (SQLException e)
        {
			e.printStackTrace();
			throw new DatabaseException(e);
        }
        catch (Exception e)
        {
			e.printStackTrace();
			throw new DatabaseException(e);
        }
            
        return user;
	}

	/**
	 * Determine if email already exists - unique (key)
	 * @param email String - email to validate
	 * @return boolean - exists(true), not exists(false)
	 */
	@Override
	public boolean emailExists(String email)
	{
        int amount = 0;

        try (Connection connection = getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SQL_SELECT_USER_EMAIL_EXISTS);)
        {
            preparedStatement.setString(1, email);

            ResultSet rs = preparedStatement.executeQuery();

            while (rs.next())
            {
            	amount  = rs.getInt("amount");
            }
        }
        catch (SQLException e)
        {
			e.printStackTrace();
			throw new DatabaseException(e);
        }
        catch (Exception e)
        {
			e.printStackTrace();
			throw new DatabaseException(e);
        }
            
        return (amount > 0);
	}
}
