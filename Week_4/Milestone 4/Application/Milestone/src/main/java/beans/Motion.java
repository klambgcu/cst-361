package beans;

import java.sql.Timestamp;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 * ---------------------------------------------------------------
 * Name      : Group 1 (Kelly Lamb, Brian Cantrell)
 * Date      : 2022-02-14
 * Class     : CST-361 Design Patterns with Java
 * Professor : Mohamed Mneimneh
 * Assignment: Milestone (Motion Devices)
 * Disclaimer: This is our own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Class defining a motion event from device(s)
 * 2.
 * 3.
 * ---------------------------------------------------------------
 */

@ManagedBean
@ViewScoped
public class Motion
{
	private int id;
	private int device_id;
	private Timestamp triggered_timestamp;
	
	/**
	 * Default Constructor
	 */
	public Motion() {}

	/**
	 * @param id integer key
	 * @param device_id integer device key
	 * @param triggered_timestamp Timestamp motion event occurred
	 */
	public Motion(int id, int device_id, Timestamp triggered_timestamp)
	{
		this.id = id;
		this.device_id = device_id;
		this.triggered_timestamp = triggered_timestamp;
	}

	/**
	 * @return the id
	 */
	public int getId()
	{
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id)
	{
		this.id = id;
	}

	/**
	 * @return the device_id
	 */
	public int getDevice_id()
	{
		return device_id;
	}

	/**
	 * @param device_id the device_id to set
	 */
	public void setDevice_id(int device_id)
	{
		this.device_id = device_id;
	}

	/**
	 * @return the triggered_timestamp
	 */
	public Timestamp getTriggered_timestamp()
	{
		return triggered_timestamp;
	}

	/**
	 * @param triggered_timestamp the triggered_timestamp to set
	 */
	public void setTriggered_timestamp(Timestamp triggered_timestamp)
	{
		this.triggered_timestamp = triggered_timestamp;
	}
}
