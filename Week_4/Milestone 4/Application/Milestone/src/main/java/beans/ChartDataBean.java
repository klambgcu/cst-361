package beans;  
import javax.annotation.PostConstruct;  
import java.io.Serializable;  
import javax.faces.bean.ManagedBean;  
import org.primefaces.model.chart.Axis;  
import org.primefaces.model.chart.AxisType;  
import org.primefaces.model.chart.CategoryAxis;  
import org.primefaces.model.chart.LineChartModel;  
import org.primefaces.model.chart.LineChartSeries;  

@ManagedBean  
public class ChartDataBean implements Serializable {  
	private LineChartModel drawArea;  

	@PostConstruct  
	public void init() {  
		createDrawArea();  
	}
	
	public LineChartModel getDrawArea() {  
		return drawArea;  
	}
	
	private void createDrawArea() {  
		drawArea = new LineChartModel();  
		LineChartSeries device = new LineChartSeries();  
		
		device.setFill(true);  
		device.setLabel("Device 1");  
		device.set("15:54", 3);  
		device.set("15:55", 1);  
		device.set("15:56", 3);  
		device.set("15:57", 10);  
		device.set("15:58", 4);  
		 
		drawArea.addSeries(device);  
		drawArea.setTitle("Motion Report");  
		drawArea.setLegendPosition("ne");  
		drawArea.setStacked(true);  
		drawArea.setShowPointLabels(true);  
		
		Axis xAxis = new CategoryAxis("Time Stamp");  
		
		drawArea.getAxes().put(AxisType.X, xAxis);  
		
		Axis yAxis = drawArea.getAxis(AxisType.Y);  
		
		yAxis.setLabel("Motion Tics");  
		yAxis.setMin(0); 

		// Set to highest motion count from devices
		yAxis.setMax(30);  
	}  
}  