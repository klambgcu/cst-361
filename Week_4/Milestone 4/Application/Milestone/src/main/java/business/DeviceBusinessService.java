package business;

import java.util.List;

import beans.Device;
import data.DatabaseInterface;
import data.DatabaseService;

/**
 * ---------------------------------------------------------------
 * Name      : Group 1 (Kelly Lamb, Brian Cantrell)
 * Date      : 2022-02-14
 * Class     : CST-361 Design Patterns with Java
 * Professor : Mohamed Mneimneh
 * Assignment: Milestone (Motion Devices)
 * Disclaimer: This is our own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Class to handle all business processes for devices
 * 2. 
 * 3.
 * ---------------------------------------------------------------
 */

public class DeviceBusinessService implements DeviceBusinessInterface
{
    private final DatabaseInterface db = new DatabaseService();

	/**
	 * Default Constructor
	 */
	public DeviceBusinessService() {}

	@Override
	public List<Device> readAllDevices()
	{
		return db.readAllDevices();
	}

	@Override
	public boolean createDevice(Device device)
	{
		return db.createDevice(device);
	}

	@Override
	public Device selectDeviceById(int id)
	{
		return db.selectDeviceById(id);
	}

	@Override
	public boolean deleteDevice(Device device)
	{
		return db.deleteDevice(device);
	}

	@Override
	public boolean updateDevice(Device device)
	{
		return db.updateDevice(device);
	}
}
