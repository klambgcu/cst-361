package business;

import java.util.List;

import beans.Console;
import data.DatabaseInterface;
import data.DatabaseService;


/**
 * ---------------------------------------------------------------
 * Name      : Group 1 (Kelly Lamb, Brian Cantrell)
 * Date      : 2022-02-21
 * Class     : CST-361 Design Patterns with Java
 * Professor : Mohamed Mneimneh
 * Assignment: Milestone (Motion Devices)
 * Disclaimer: This is our own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Class to handle all business processes for the console app
 * 2. 
 * 3.
 * ---------------------------------------------------------------
 */

public class ConsoleBusinessService implements ConsoleBusinessInterface
{
    private final DatabaseInterface db = new DatabaseService();

	/**
	 * Default Constructor
	 */
	public ConsoleBusinessService() {}

	@Override
	public List<Console> getUsersWithDevices()
	{
		return db.getUsersWithDevices();
	}

	@Override
	public List<Console> getUsersWithDevices(int user_id)
	{
		return db.getUsersWithDevices(user_id);
	}
}
