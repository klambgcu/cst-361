package business;

import java.util.List;

import beans.Device;

/**
 * ---------------------------------------------------------------
 * Name      : Group 1 (Kelly Lamb, Brian Cantrell)
 * Date      : 2022-02-14
 * Class     : CST-361 Design Patterns with Java
 * Professor : Mohamed Mneimneh
 * Assignment: Milestone (Motion Devices)
 * Disclaimer: This is our own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Interface to house all business processes for devices
 * 2. 
 * 3.
 * ---------------------------------------------------------------
 */

public interface DeviceBusinessInterface
{
    /**
     * 
     * @return List : All devices from the database
     */
    public List<Device> readAllDevices();
    
    /**
     * 
     * @param device : Device to insert into the database
     * @return boolean: true(successful), false otherwise
     */
    public boolean createDevice(Device device);
    
    /**
     * 
     * @param id: Integer ID (Key) to select device
     * @return Device: User object if found otherwise null
     */
    public Device selectDeviceById(int id);
    
    /**
     * 
     * @param device : Device model to delete from database (ID)
     * @return boolean: true(successful), false otherwise
     */
    public boolean deleteDevice(Device device);
    
    /**
     * 
     * @param device : Device object to update
     * @return boolean: true(successful), false otherwise
     */
    public boolean updateDevice(Device device);

}
