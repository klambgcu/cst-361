package com.motiondevices;

import java.util.List;

public interface DataList
{
	List<DTO> getList();
}
