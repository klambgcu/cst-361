package com.motiondevices;

/**
 * ---------------------------------------------------------------
 * Name      : Group 1 (Kelly Lamb, Brian Cantrell)
 * Date      : 2022-02-21
 * Class     : CST-361 Design Patterns with Java
 * Professor : Mohamed Mneimneh
 * Assignment: Milestone (Motion Devices)
 * Disclaimer: This is our own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Base class to call RESTful APIS
 * 2. getusers
 * 3. getdevices/{id}
 * ---------------------------------------------------------------
 */

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import com.fasterxml.jackson.databind.ObjectMapper;

public class DataListCallAPI
{
	public DataListCallAPI() {}

	public ResponseDataModel callAPI(String apiURL)
	{
		/* http://localhost:8080/Milestone/rest/console/getusers */
		/* http://localhost:8080/Milestone/rest/console/getdevices/4 */

		StringBuilder response = new StringBuilder();
		ResponseDataModel data = new ResponseDataModel(0,"",null);

		try
		{
			URL url = new URL(apiURL);

			HttpURLConnection con = (HttpURLConnection) url.openConnection();

			con.setRequestMethod("GET");
			con.setRequestProperty("Content-Type", "application/json");
			con.setConnectTimeout(5000);
			con.setReadTimeout(5000);
			con.setDoOutput(false); // set to true to pass parameters

			int code = con.getResponseCode();

			try(BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream(), "utf-8")))
			{
				String responseLine = null;
				while ((responseLine = br.readLine()) != null)
				{
					response.append(responseLine.trim());
				}
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}

			try
			{
				String input = response.toString();
				ObjectMapper om = new ObjectMapper();
				data = om.readValue(input, ResponseDataModel.class);
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}

		return data;
	}
}