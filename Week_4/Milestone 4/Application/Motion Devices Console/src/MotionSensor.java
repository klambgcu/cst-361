package com.motiondevices;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.BorderFactory;
import javax.swing.JLabel;

public class MotionSensor extends JLabel
{
	Dimension minSize = new Dimension(40, 40);
	int id;
	String name;

	public MotionSensor(int id, String name)
	{
		setBackground(Color.LIGHT_GRAY);
		setOpaque(true);
		setBorder(BorderFactory.createLineBorder(Color.black));
		setText(name);
		this.name = name;
		this.id = id;

		addMouseListener(new MouseAdapter()
		{
		    @Override
		    public void mouseEntered(MouseEvent e)
		    {
		        setBackground(Color.YELLOW);
		    }
		    @Override
		    public void mouseExited(MouseEvent e)
		    {
		        setBackground(Color.LIGHT_GRAY);
		    }
		});
	}

	public Dimension getMinimumSize()
	{
		return minSize;
	}

	public Dimension getPreferredSize()
	{
		return minSize;
	}

	public int getId()
	{
		return id;
	}

	public String getName()
	{
		return name;
	}
}
