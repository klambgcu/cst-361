package com.motiondevices;

public class DTO
{
	private int id;
	private String displayName;

	public DTO(int id, String displayName)
	{
		this.id = id;
		this.displayName = displayName;
	}

	public int getId()
	{
		return this.id;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public String getDisplayName()
	{
		return this.displayName;
	}

	public void setDisplayName(String displayName)
	{
		this.displayName = displayName;
	}

	@Override
	public String toString()
	{
		return this.displayName;
	}
}
