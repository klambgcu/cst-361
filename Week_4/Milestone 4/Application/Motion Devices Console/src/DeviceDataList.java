package com.motiondevices;

import java.util.ArrayList;
import java.util.List;
import java.sql.*;

public class DeviceDataList implements DataList
{
	private List<DTO> list = new ArrayList<DTO>();
	private DataListCallAPI api = new DataListCallAPI();

	public DeviceDataList(int user_id)
	{
		ResponseDataModel data = api.callAPI("http://localhost:8080/Milestone/rest/console/getdevices/" + user_id);

		for (Console item : data.getData())
		{
			//System.out.println(item.getId() + item.getDisplayName());
			list.add( new DTO( item.getId(), item.getDisplayName() ) );
		}

		/*******************************************************
		Replace SQL with API Calls

		// Do SQL Query
		// select id, name from devices where user_id = 4 and activated = true;

		try
		{
			Class.forName("com.mysql.cj.jdbc.Driver");
			Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/cst-361","root","root");
			PreparedStatement preparedStatement = connection.prepareStatement("select id, name from devices where user_id = ? and activated = 1 order by id");

			preparedStatement.setInt(1, user_id);

			ResultSet rs = preparedStatement.executeQuery();

			while (rs.next())
			{
				int device_id = rs.getInt("id");
				String name   = rs.getString("name");
				String displayName = name + " (" + device_id + ")";
				list.add( new DTO(device_id, displayName) );
			}

			connection.close();
		}
		catch(Exception e)
		{
			System.out.println(e);
		}


		********************************************************/
	}

	public List<DTO> getList()
	{
		return this.list;
	}
}
