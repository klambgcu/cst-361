package shapes;

/**
 * ---------------------------------------------------------------
 * Name      : Kelly Lamb
 * Date      : 2022-03-14
 * Class     : CST-361 Design Patterns with Java
 * Professor : Mohamed Mneimneh
 * Assignment: Activity 7c2
 * Disclaimer: This is our own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Class to represent a Square shape
 * 2.
 * 3.
 * ---------------------------------------------------------------
 */

public class Square extends ShapeAbstract
{
	int size = 1;
	
	public Square() {}
	
	@Override
	public void draw()
	{
		System.out.println("Square::draw() Position[X: " + x + ", Y: " + y + "] Size: " + Math.round(size * scale) + " Color: " + color);
	}
}
