package shapes;

/**
 * ---------------------------------------------------------------
 * Name      : Kelly Lamb
 * Date      : 2022-03-14
 * Class     : CST-361 Design Patterns with Java
 * Professor : Mohamed Mneimneh
 * Assignment: Activity 7c3
 * Disclaimer: This is our own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Class to decorate shape color
 * 2.
 * 3.
 * ---------------------------------------------------------------
 */

public class BorderColorShapeDecorator extends ShapeDecoratorAbstract
{
	// Default border color 
	private String borderColor = "Black";
	
	public BorderColorShapeDecorator(ShapeInterface decoratedShape)
	{
		super(decoratedShape);
	}

	public BorderColorShapeDecorator(ShapeInterface decoratedShape, String borderColor)
	{
		super(decoratedShape);
		setBorderColor(borderColor);
	}
	
	@Override
	public void draw()
	{
		decoratedShape.draw();
		System.out.println("Border Color: " + this.borderColor);
	}
	
	private void setBorderColor(String borderColor)
	{
		this.borderColor = borderColor;
		System.out.println("Setting Border Color to: " + borderColor);
	}

}
