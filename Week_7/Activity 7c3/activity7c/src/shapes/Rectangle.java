package shapes;

/**
 * ---------------------------------------------------------------
 * Name      : Kelly Lamb
 * Date      : 2022-03-14
 * Class     : CST-361 Design Patterns with Java
 * Professor : Mohamed Mneimneh
 * Assignment: Activity 7c2
 * Disclaimer: This is our own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Class to represent a Rectangle shape
 * 2.
 * 3.
 * ---------------------------------------------------------------
 */

public class Rectangle extends ShapeAbstract
{
	int sizeH = 2;
	int sizeV = 1;
	
	public Rectangle() {}
	
	@Override
	public void draw()
	{
		System.out.println("Rectangle::draw() Position[X: " + x + ", Y: " + y + "] Horizontal Size: " + Math.round(sizeH * scale) + " Vertical Size: " + Math.round(sizeV * scale) + " Color: " + color);
	}
}
