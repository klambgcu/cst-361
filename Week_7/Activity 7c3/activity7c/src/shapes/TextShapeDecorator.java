package shapes;

/**
 * ---------------------------------------------------------------
 * Name      : Kelly Lamb
 * Date      : 2022-03-14
 * Class     : CST-361 Design Patterns with Java
 * Professor : Mohamed Mneimneh
 * Assignment: Activity 7c3
 * Disclaimer: This is our own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Class to decorate shape text field
 * 2.
 * 3.
 * ---------------------------------------------------------------
 */

public class TextShapeDecorator extends ShapeDecoratorAbstract
{
	// Default text message
	private String text = "Shape";
	
	public TextShapeDecorator(ShapeInterface decoratedShape)
	{
		super(decoratedShape);
	}
	
	public TextShapeDecorator(ShapeInterface decoratedShape, String text)
	{
		super(decoratedShape);
		setText(text);
	}
	
	@Override
	public void draw()
	{
		decoratedShape.draw();
		System.out.println("Text: " + this.text);
	}
	
	private void setText(String text)
	{
		this.text = text;
		System.out.println("Setting Text to: " + text);
	}
}
