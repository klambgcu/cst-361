package shapes;

/**
 * ---------------------------------------------------------------
 * Name      : Kelly Lamb
 * Date      : 2022-03-14
 * Class     : CST-361 Design Patterns with Java
 * Professor : Mohamed Mneimneh
 * Assignment: Activity 7c3
 * Disclaimer: This is our own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Abstract Class to decorate shapes
 * 2.
 * 3.
 * ---------------------------------------------------------------
 */

public abstract class ShapeDecoratorAbstract implements ShapeInterface
{
	protected ShapeInterface decoratedShape;

	public ShapeDecoratorAbstract(ShapeInterface decoratedShape)
	{
		this.decoratedShape = decoratedShape;
	}

	@Override
	public void draw()
	{
		decoratedShape.draw();
	}

	@Override
	public void setPosition(int x, int y)
	{
		decoratedShape.setPosition(x, y);
	}

	@Override
	public void setScale(double scale)
	{
		decoratedShape.setScale(scale);
	}

	@Override
	public void setColor(String color)
	{
		decoratedShape.setColor(color);
	}
}
