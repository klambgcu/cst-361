package shapes;

/**
 * ---------------------------------------------------------------
 * Name      : Kelly Lamb
 * Date      : 2022-03-14
 * Class     : CST-361 Design Patterns with Java
 * Professor : Mohamed Mneimneh
 * Assignment: Activity 7c2
 * Disclaimer: This is our own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Interface to represent a shape
 * 2.
 * 3.
 * ---------------------------------------------------------------
 */

public interface ShapeInterface
{
	void draw();
	void setPosition(int x, int y);
	void setScale(double scale);
	void setColor(String color);
}
