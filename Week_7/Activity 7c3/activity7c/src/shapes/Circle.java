package shapes;

/**
 * ---------------------------------------------------------------
 * Name      : Kelly Lamb
 * Date      : 2022-03-14
 * Class     : CST-361 Design Patterns with Java
 * Professor : Mohamed Mneimneh
 * Assignment: Activity 7c2
 * Disclaimer: This is our own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Class to represent a Circle shape
 * 2.
 * 3.
 * ---------------------------------------------------------------
 */

public class Circle extends ShapeAbstract
{
	int radius = 1;
	
	@Override
	public void draw()
	{
		System.out.println("Circle::draw() Position[X: " + x + ", Y: " + y + "] Radius: " + Math.round(radius * scale) + " Color: " + color);
	}	
}