package shapes;

/**
 * ---------------------------------------------------------------
 * Name      : Kelly Lamb
 * Date      : 2022-03-14
 * Class     : CST-361 Design Patterns with Java
 * Professor : Mohamed Mneimneh
 * Assignment: Activity 7c2
 * Disclaimer: This is our own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Factory class to instantiate all known shape types
 * 2.
 * 3.
 * ---------------------------------------------------------------
 */

public class ShapeFactory
{
	public static enum DecoratorType 
	{
		BorderColor,
		Text
	}
	
	public ShapeFactory() {}
	
	public ShapeInterface getShape(String type)
	{
		ShapeInterface shape = null;

		if (null == type) return null;
		
		switch(type.toLowerCase())
		{
			case "square" :    { shape = new Square(); break; }
			case "rectangle" : { shape = new Rectangle(); break; }
			case "circle" :    { shape = new Circle(); break; }
		}
		
		return shape;
	}
	
	public ShapeInterface getShape(DecoratorType type, ShapeInterface shape, String value)
	{
		ShapeInterface decoratedShape = null;
		
		if (null == shape) return null;
		
		switch(type)
		{
			case BorderColor :
			{
				decoratedShape = new BorderColorShapeDecorator(shape, value);
				break;
			}
			case Text :
			{
				decoratedShape = new TextShapeDecorator(shape, value);
				break;
			}
		}
		
		return decoratedShape;
	}
}
