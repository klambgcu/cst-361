package shapes;

/**
 * ---------------------------------------------------------------
 * Name      : Kelly Lamb
 * Date      : 2022-03-14
 * Class     : CST-361 Design Patterns with Java
 * Professor : Mohamed Mneimneh
 * Assignment: Activity 7c2,3
 * Disclaimer: This is our own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Main Driver Class
 * 2. Test Functionality of: Factory and Decorators
 * 3.
 * ---------------------------------------------------------------
 */

public class ShapeFactoryTester
{
	public static void main(String[] args)
	{
		ShapeFactoryTester app = new ShapeFactoryTester();
		app.run();
	}
	
	public void run()
	{
		ShapeFactory factory = new ShapeFactory();

		// Test Square Shape
		System.out.println("*********************** SQUARE ****************************");
		ShapeInterface shape = factory.getShape("square");
		shape.setColor("Green");
		shape.setPosition(getRandomInt(2000), getRandomInt(2000));
		shape.setScale(getRandomDouble(1000.0));

		// Add Decorators
		shape = factory.getShape(ShapeFactory.DecoratorType.BorderColor, shape, "Yellow");
		shape = factory.getShape(ShapeFactory.DecoratorType.Text, shape, "SQUARE");
		shape.draw();
		
		// Test Rectangle Shape
		System.out.println("\n*********************** RECTANGLE ****************************");
		shape = factory.getShape("rectangle");
		shape.setColor("Purple");
		shape.setPosition(getRandomInt(2000), getRandomInt(2000));
		shape.setScale(getRandomDouble(1000.0));

		// Add Decorators
		shape = factory.getShape(ShapeFactory.DecoratorType.BorderColor, shape, "Blue");
		shape = factory.getShape(ShapeFactory.DecoratorType.Text, shape, "RECTANGLE");
		shape.draw();
		
		// Test Circle Shape
		System.out.println("\n*********************** CIRCLE ****************************");
		shape = factory.getShape("circle");
		shape.setColor("Red");
		shape.setPosition(getRandomInt(2000), getRandomInt(2000));
		shape.setScale(getRandomDouble(1000.0));

		// Add Decorators
		shape = factory.getShape(ShapeFactory.DecoratorType.BorderColor, shape, "Green");
		shape = factory.getShape(ShapeFactory.DecoratorType.Text, shape, "CIRCLE");
		shape.draw();
	}
	
	public double getRandomDouble(double max)
	{
		return (double)(Math.random() * max );
	}

	public int getRandomInt(int max)
	{
		return (int)(Math.random() * max);
	}
}
