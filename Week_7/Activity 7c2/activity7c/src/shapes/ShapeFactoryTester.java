package shapes;

public class ShapeFactoryTester
{
	public static void main(String[] args)
	{
		ShapeFactoryTester app = new ShapeFactoryTester();
		app.run();
	}
	
	public void run()
	{
		ShapeFactory factory = new ShapeFactory();

		// Test Square Shape
		System.out.println("*********************** SQUARE ****************************");
		ShapeInterface shape = factory.getShape("square");
		shape.setColor("Green");
		shape.setPosition(getRandomInt(2000), getRandomInt(2000));
		shape.setScale(getRandomDouble(1000.0));
		shape.draw();
		
		// Test Rectangle Shape
		System.out.println("\n*********************** RECTANGLE ****************************");
		shape = factory.getShape("rectangle");
		shape.setColor("Purple");
		shape.setPosition(getRandomInt(2000), getRandomInt(2000));
		shape.setScale(getRandomDouble(1000.0));
		shape.draw();
		
		// Test Circle Shape
		System.out.println("\n*********************** CIRCLE ****************************");
		shape = factory.getShape("circle");
		shape.setColor("Red");
		shape.setPosition(getRandomInt(2000), getRandomInt(2000));
		shape.setScale(getRandomDouble(1000.0));
		shape.draw();
	}
	
	public double getRandomDouble(double max)
	{
		return (double)(Math.random() * max );
	}

	public int getRandomInt(int max)
	{
		return (int)(Math.random() * max);
	}
	
}
