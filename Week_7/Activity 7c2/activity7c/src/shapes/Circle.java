package shapes;

/**
 * @author Kelly Lamb
 *
 */
public class Circle extends ShapeAbstract
{
	int radius = 1;
	
	@Override
	public void draw()
	{
		System.out.println("Circle::draw() Position[X: " + x + ", Y: " + y + "] Radius: " + Math.round(radius * scale) + " Color: " + color);
	}	
}