package shapes;

/**
 * @author Kelly Lamb
 *
 */
public class Rectangle extends ShapeAbstract
{
	int sizeH = 2;
	int sizeV = 1;
	
	public Rectangle() {}
	
	@Override
	public void draw()
	{
		System.out.println("Rectangle::draw() Position[X: " + x + ", Y: " + y + "] Horizontal Size: " + Math.round(sizeH * scale) + " Vertical Size: " + Math.round(sizeV * scale) + " Color: " + color);
	}
}
