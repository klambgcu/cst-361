package shapes;

public class ShapeFactory
{
	public ShapeFactory() {}
	
	public ShapeInterface getShape(String type)
	{
		ShapeInterface shape = null;

		if (null == type) return null;
		
		switch(type.toLowerCase())
		{
			case "square" :    { shape = new Square(); break; }
			case "rectangle" : { shape = new Rectangle(); break; }
			case "circle" :    { shape = new Circle(); break; }
		}
		
		return shape;
	}
}
