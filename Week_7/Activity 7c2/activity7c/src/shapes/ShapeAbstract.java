package shapes;

public abstract class ShapeAbstract implements ShapeInterface
{
	int x = 0;
	int y = 0;
	double scale = 1.0;
	String color = "white";

	public ShapeAbstract() {}

	@Override
	public abstract void draw();

	@Override
	public void setPosition(int x, int y)
	{
		this.x = x;
		this.y = y;
		System.out.println("Setting Position[X: " + x + ", Y:" + y + "]");
	}

	@Override
	public void setScale(double scale)
	{
		this.scale = scale;
		System.out.println("Setting Scale: " + Math.round(scale));
	}

	@Override
	public void setColor(String color)
	{
		this.color = color;
		System.out.println("Setting Color: " + color);
	}
}
