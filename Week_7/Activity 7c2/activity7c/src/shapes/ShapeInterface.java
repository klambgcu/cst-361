package shapes;

public interface ShapeInterface
{
	void draw();
	void setPosition(int x, int y);
	void setScale(double scale);
	void setColor(String color);
}
