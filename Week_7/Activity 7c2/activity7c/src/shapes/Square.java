package shapes;

/**
 * @author Kelly Lamb
 *
 */
public class Square extends ShapeAbstract
{
	int size = 1;
	
	public Square() {}
	
	@Override
	public void draw()
	{
		System.out.println("Square::draw() Position[X: " + x + ", Y: " + y + "] Size: " + Math.round(size * scale) + " Color: " + color);
	}
}
