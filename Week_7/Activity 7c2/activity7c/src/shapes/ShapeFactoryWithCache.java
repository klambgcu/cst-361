package shapes;

import java.util.HashMap;

public class ShapeFactoryWithCache
{
	// This type of facShapeFactoryWithCachetory creates only three shapes and stores them in a cache.
	// Efficient to pull from cache rather than instantiate constantly.
	// However, this means the objects are reused and potentially reused by other threads
	private HashMap<String, ShapeInterface> cache = new HashMap<String, ShapeInterface>();

	public ShapeFactoryWithCache() {}
	
	public ShapeInterface getShape(String type)
	{
		ShapeInterface shape = null;

		if (null == type) return null;
		
		switch(type.toLowerCase())
		{
			case "square" :
			{
				shape = cache.get("square");
				
				if (null == shape)
				{
					shape = new Square();
					cache.put("square", shape);
				}			
				break;
			}
			case "rectangle" :
			{
				shape = cache.get("rectangle");
				
				if (null == shape)
				{
					shape = new Rectangle();
					cache.put("rectangle", shape);
				}			
				break;
			}
			case "circle" : 
			{
				shape = cache.get("circle");
				
				if (null == shape)
				{
					shape = new Circle();
					cache.put("circle", shape);
				}			
				break;
			}
		}
		
		return shape;
	}
}
