package business;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.ejb.Local;
import javax.ejb.LocalBean;
import javax.enterprise.inject.Alternative;
import javax.interceptor.Interceptors;

import beans.Album;
import beans.Track;
import interceptors.LoggingInterceptor;

/**
 * ---------------------------------------------------------------
 * Name      : Kelly Lamb
 * Date      : 2022-02-28
 * Class     : CST-361 Design Patterns with Java
 * Professor : Mohamed Mneimneh
 * Assignment: Activity 7a
 * Disclaimer: This is our own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Concrete class to handle album track information finding
 * 2.
 * 3.
 * ---------------------------------------------------------------
 */

@Alternative
@Local(TrackFinderInterface.class)
@LocalBean
@Interceptors(LoggingInterceptor.class)
public class TrackFinder1 implements TrackFinderInterface
{
	// Combination of Artist + Album Title + Year that stores a List of Tracks
	private HashMap<String, List<Track>> TrackInfo;

	public TrackFinder1()
	{
        TrackInfo = new HashMap<String, List<Track>>();
    	//service = new MusicDataService();

        // Add Journey Escape Album
        String key = "JourneyEscape1981";
        List<Track> list = new ArrayList<Track>();
        list.add(new Track("Don't Stop Believin'", 1));
        list.add(new Track("Stone in Love", 2));
        list.add(new Track("Who's Crying Now",3));
        list.add(new Track("Keep On Runnin'", 4));
        list.add(new Track("Still They Ride",5));
        list.add(new Track("Escape", 6));
        list.add(new Track("Lay It Down", 7));
        list.add(new Track("Dead or Alive", 8));
        list.add(new Track("Mother, Father", 9));
        list.add(new Track("Open Arms", 10));        
        TrackInfo.put(key, list);
        
        // Add The Cure Wishes Album
        key = "The CureWishes1992";
        list = new ArrayList<Track>();
        list.add(new Track("Open", 1));
        list.add(new Track("High", 2));
        list.add(new Track("Apart", 3));
        list.add(new Track("From the Edge of the Deep Green Sea", 4));
        list.add(new Track("Wendy Time", 5));
        list.add(new Track("Doing the Unstuck", 6));
        list.add(new Track("Friday I'm in Love", 7));
        list.add(new Track("Trust", 8));
        list.add(new Track("A Letter to Elise", 9));
        list.add(new Track("Cut", 10));
        list.add(new Track("To Wish Impossible Things", 11));
        list.add(new Track("End", 12));
        TrackInfo.put(key, list);		
        
        key="The FixxReach The Beach1983";
        list = new ArrayList<Track>();
        list.add(new Track("One Thing Leads to Another",1));
        list.add(new Track("The Sign of Fire",2));
        list.add(new Track("Running",3));
        list.add(new Track("Saved by Zero",4));
        list.add(new Track("Opinions",5));
        list.add(new Track("Reach the Beach",6));
        list.add(new Track("Changing",7));
        list.add(new Track("Liner",8));
        list.add(new Track("Privilege",9));
        list.add(new Track("Outside",10));
        TrackInfo.put(key, list);		
	}

	@Override
	public List<Track> getTracks(Album album)
	{
		String key = album.getArtist().trim() + album.getTitle().trim() + Integer.toString(album.getYear()).trim();
		return TrackInfo.get(key);
	}

}
