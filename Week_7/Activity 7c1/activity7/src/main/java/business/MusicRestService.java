package business;

import javax.ejb.EJB;
import javax.interceptor.Interceptors;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import beans.Album;
import beans.ResponseDataModel;
import interceptors.LoggingInterceptor;
import util.AlbumNotFoundException;

/**
 * ---------------------------------------------------------------
 * Name      : Kelly Lamb
 * Date      : 2022-02-14
 * Class     : CST-361 Design Patterns with Java
 * Professor : Mohamed Mneimneh
 * Assignment: Activity 4
 * Disclaimer: This is our own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Class defining a RESTful service for album information
 * 2. JSON getalbum j
 * 3. XML  getalbum x
 * ---------------------------------------------------------------
 */

@Path("/music")
@Interceptors(LoggingInterceptor.class)
public class MusicRestService
{
	@EJB
	MusicManagerInterface service;
	
	@GET
	@Path("/getalbumj/{title}/{artist}/{year}")
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseDataModel getAlbumj( @PathParam("title") String title, @PathParam("artist") String artist, @PathParam("year") int year )
	{
		//MusicManager manager = new MusicManager();
		Album album = null;
		int status = 0;
		String message = "";
		
		try
		{
			Album model = new Album(title, artist, year);
			album = service.getAlbum(model);
		}
		catch (AlbumNotFoundException e)
		{
			System.out.println(e.getMessage());
			status = 404;
			message = e.getMessage();
		}
		catch (Exception e)
		{
			System.out.println(e.getMessage());			
			status = 500;
			message = "An error has occurred. Please contact administrator.";
		}
		
		return new ResponseDataModel(status, message, album);
	}
	
	@GET
	@Path("/getalbumx/{title}/{artist}/{year}")
	@Produces({ MediaType.APPLICATION_XML })
	public ResponseDataModel getAlbumx( @PathParam("title") String title, @PathParam("artist") String artist, @PathParam("year") int year )
	{
		//MusicManager manager = new MusicManager();
		Album album = null;
		int status = 0;
		String message = "";
		
		try
		{
			Album model = new Album(title, artist, year);
			album = service.getAlbum(model);
		}
		catch (AlbumNotFoundException e)
		{
			System.out.println(e.getMessage());
			status = 404;
			message = e.getMessage();
		}
		catch (Exception e)
		{
			System.out.println(e.getMessage());			
			status = 500;
			message = "An error has occurred. Please contact administrator.";
		}
		
		return new ResponseDataModel(status, message, album);
	}
}
