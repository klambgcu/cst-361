package business;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;

import javax.ejb.Local;
import javax.ejb.LocalBean;
import javax.enterprise.inject.Alternative;
import javax.interceptor.Interceptors;

import beans.Album;
import beans.Track;
import interceptors.LoggingInterceptor;

/**
 * ---------------------------------------------------------------
 * Name      : Kelly Lamb
 * Date      : 2022-02-28
 * Class     : CST-361 Design Patterns with Java
 * Professor : Mohamed Mneimneh
 * Assignment: Activity 7a
 * Disclaimer: This is our own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Concrete class to handle album track information finding
 * 2.
 * 3.
 * ---------------------------------------------------------------
 */

@Alternative
@Local(TrackFinderInterface.class)
@LocalBean
@Interceptors(LoggingInterceptor.class)
public class TrackFinder2 implements TrackFinderInterface
{
	// Combination of Artist + Album Title + Year that stores a List of Tracks
	private HashMap<String, List<Track>> TrackInfo;

	public TrackFinder2()
	{
        TrackInfo = new HashMap<String, List<Track>>();
        String key = "";
        List<Track> list = new ArrayList<Track>();
   
        try
        {
            Scanner scanner = new Scanner(this.getClass().getResourceAsStream("/music.txt"));
        	while (scanner.hasNextLine())
        	{
        		String line = scanner.nextLine();
        		// System.out.println("MUSIC: " + line);
        		
        		if (line.startsWith("A")) // Album
        		{
        			if (! key.equals(""))
        			{
        				TrackInfo.put(key, list);
        				list = new ArrayList<Track>();
        			}
        			
        			String[] info = line.split("\t");
        			key = info[1].trim() + info[2].trim() + info[3].trim();
        		}
        		else if (line.startsWith("T")) // Track for current album
        		{
        			String[] info = line.split("\t");
        			list.add( new Track (info[1], Integer.valueOf(info[2])) );
        		}
        	}
        	// End of file - add remaining list to track info map
			TrackInfo.put(key, list);
        	
        }
        catch(Exception e)
        {
        	System.out.println("music.txt file not found in resources");
        }
	}

	@Override
	public List<Track> getTracks(Album album)
	{
		String key = album.getArtist().trim() + album.getTitle().trim() + Integer.toString(album.getYear()).trim();
		return TrackInfo.get(key);
	}

}
