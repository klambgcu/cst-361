package business;

import java.util.List;

import beans.Album;
import beans.Track;


/**
 * ---------------------------------------------------------------
 * Name      : Kelly Lamb
 * Date      : 2022-02-28
 * Class     : CST-361 Design Patterns with Java
 * Professor : Mohamed Mneimneh
 * Assignment: Activity 7a
 * Disclaimer: This is our own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Interface to handle album track information finding
 * 2.
 * 3.
 * ---------------------------------------------------------------
 */

public interface TrackFinderInterface
{
	List<Track> getTracks(Album album);
}