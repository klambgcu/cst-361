package beans;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * ---------------------------------------------------------------
 * Name      : Kelly Lamb
 * Date      : 2022-02-14
 * Class     : CST-361 Design Patterns with Java
 * Professor : Mohamed Mneimneh
 * Assignment: Activity 4
 * Disclaimer: This is our own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Class defining a music album
 * 2.
 * 3.
 * ---------------------------------------------------------------
 */

@ManagedBean
@ViewScoped
public class Album
{
	@NotNull(message="Title cannot be empty")
	@Size(min=5, max=50, message="Title length must be between 5-50 characters") 
	private String title;
	
	@NotNull(message="Artist cannot be empty")
	@Size(min=5, max=25, message="Artist length must be between 5-25 characters") 
	private String artist;

	@Min(value = 1920, message="Year cannot be less than 1920")
	@Max(value = 2020, message="Year cannot be greater than 2020")
	@NotNull(message="Year cannot be empty")
	private int year;

	private List<Track> tracks;

	/**
	 * Default Constructor
	 */
	public Album()
	{
		this.title = "";
		this.artist = "";
		this.year = 0;
		this.tracks = new ArrayList<Track>();
	}

	/**
	 * @param title String title of album
	 * @param artist String name of the artist or musical group
	 * @param year int year album was released
	 *  
	 */
	public Album(String title, String artist, int year)
	{
		this.title = title;
		this.artist = artist;
		this.year = year;
		this.tracks = new ArrayList<Track>();
	}
	
	/**
	 * @return the title
	 */
	public int getNumberTracks()
	{
		return this.tracks.size();
	}

	/**
	 * @return the title
	 */
	public String getTitle()
	{
		return title;
	}

	/**
	 * @param title the title to set
	 */
	public void setTitle(String title)
	{
		this.title = title;
	}

	/**
	 * @return the artist
	 */
	public String getArtist()
	{
		return artist;
	}

	/**
	 * @param artist the artist to set
	 */
	public void setArtist(String artist)
	{
		this.artist = artist;
	}

	/**
	 * @return the year
	 */
	public int getYear()
	{
		return year;
	}

	/**
	 * @param year the year to set
	 */
	public void setYear(int year)
	{
		this.year = year;
	}

	/**
	 * @return the tracks
	 */
	public List<Track> getTracks()
	{
		return tracks;
	}

	/**
	 * @param tracks the tracks to set
	 */
	public void setTracks(List<Track> tracks)
	{
		this.tracks = tracks;
	}
}
