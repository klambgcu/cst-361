package beans;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * ---------------------------------------------------------------
 * Name      : Kelly Lamb
 * Date      : 2022-02-21
 * Class     : CST-361 Design Patterns with Java
 * Professor : Mohamed Mneimneh
 * Assignment: Activity 6
 * Disclaimer: This is our own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Class defining a response DTO
 * 2.
 * 3.
 * ---------------------------------------------------------------
 */

@XmlRootElement(name = "Response")
@XmlAccessorType(XmlAccessType.FIELD) 
public class ResponseDataModel extends ResponseModel
{
	private Album data;
	
	/**
	 * Default Constructor
	 */
	public ResponseDataModel() {}

	/**
	 * Non-Default Constructor
	 * @param status integer for numeric response
	 * @param message String response for status
	 * @param data Album Music information
	 */
	public ResponseDataModel(int status, String message, Album data)
	{
		super(status, message);
		this.data = data;
	}

	/**
	 * @return the data
	 */
	public Album getData()
	{
		return data;
	}

	/**
	 * @param data the data to set
	 */
	public void setData(Album data)
	{
		this.data = data;
	}
}
