package business;

import java.util.List;

import javax.ejb.Local;
import javax.ejb.LocalBean;
import javax.interceptor.Interceptors;

import beans.Motion;
import data.DatabaseInterface;
import data.DatabaseService;
import interceptors.LoggingInterceptor;

/**
 * ---------------------------------------------------------------
 * Name      : Group 1 (Kelly Lamb, Brian Cantrell)
 * Date      : 2022-02-14
 * Class     : CST-361 Design Patterns with Java
 * Professor : Mohamed Mneimneh
 * Assignment: Milestone (Motion Motions)
 * Disclaimer: This is our own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Class to handle all business processes for motions
 * 2. 
 * 3.
 * ---------------------------------------------------------------
 */

@Local(MotionBusinessInterface.class)
@LocalBean
@Interceptors(LoggingInterceptor.class)
public class MotionBusinessService implements MotionBusinessInterface
{
    private final DatabaseInterface db = new DatabaseService();

	/**
	 * Default Constructor
	 */
	public MotionBusinessService() {}

	@Override
	public List<Motion> readAllMotions()
	{
		return db.readAllMotions();
	}

	@Override
	public boolean createMotion(Motion motion)
	{
		return db.createMotion(motion);
	}

	@Override
	public Motion selectMotionById(int id)
	{
		return db.selectMotionById(id);
	}

	@Override
	public boolean deleteMotion(Motion motion)
	{
		return db.deleteMotion(motion);
	}

	@Override
	public boolean updateMotion(Motion motion)
	{
		return db.updateMotion(motion);
	}
}
