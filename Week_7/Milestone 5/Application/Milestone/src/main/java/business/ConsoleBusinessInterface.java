package business;

import java.util.List;

import beans.Console;

/**
 * ---------------------------------------------------------------
 * Name      : Group 1 (Kelly Lamb, Brian Cantrell)
 * Date      : 2022-02-21
 * Class     : CST-361 Design Patterns with Java
 * Professor : Mohamed Mneimneh
 * Assignment: Milestone (Motion Devices)
 * Disclaimer: This is our own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Interface to house all business processes for the console app
 * 2. 
 * 3.
 * ---------------------------------------------------------------
 */

public interface ConsoleBusinessInterface
{
    /**
     * 
     * @return List : All users that have devices assigned and activated database
     */
	List<Console> getUsersWithDevices();
	
    /**
     * @param user_id : Integer id for user
     * @return List : All activated devices for specific user from the database
     */
	List<Console> getUsersWithDevices(int user_id);
}
