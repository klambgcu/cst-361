package business;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.interceptor.Interceptors;

import beans.Device;
import interceptors.LoggingInterceptor;

/**
 * ---------------------------------------------------------------
 * Name      : Kelly Lamb
 * Date      : 2022-03-19
 * Class     : CST-361 Design Patterns with Java
 * Professor : Mohamed Mneimneh
 * Assignment: Milestone
 * Disclaimer: This is our own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Concrete class to cache instances of devices
 * 2.
 * 3.
 * ---------------------------------------------------------------
 */

@Singleton
@Startup
@Interceptors(LoggingInterceptor.class)
public class CacheDevice
{
	@EJB DeviceBusinessInterface service;
	
	private HashMap<String, Device> cache;

	public CacheDevice() {}
	
	@PostConstruct
	public void init()
	{
		System.out.println("CacheDevice:init() - Entering");

		cache = new HashMap<String, Device>();
		List<Device> list = service.readAllDevices();
		
		for(Device device : list)
		{
			String key = device.getName().trim() + Integer.toString(device.getCode()).trim() + Integer.toString(device.getUser_id()).trim();
			cache.put(key, device);
		}

		System.out.println("CacheDevice:init() - Exiting");
	}

	public Device getObject(Device device)
	{
		System.out.println("CacheDevice:getObject() - Entering");
		
		String key = device.getName().trim() + Integer.toString(device.getCode()).trim() + Integer.toString(device.getUser_id()).trim();
		System.out.println("CacheDevice:getObject() - key: " + key);
		
		Device model = cache.get(key);
		String msg = (null == model) ? "Item not in the cache." : "Item found in cache.";
		System.out.println("CacheDevice:getObject() - " + msg);
		
		System.out.println("CacheDevice:getObject() - Exiting");
		return model;
	}

	public void putObject(Device device)
	{
		System.out.println("CacheDevice:putOject() - Entering");
		String key = device.getName().trim() + Integer.toString(device.getCode()).trim() + Integer.toString(device.getUser_id()).trim();
		
		System.out.println("CacheDevice:putOject() - Storing or replacing device in cache - key: " + key);
		cache.put(key, device);
		
		System.out.println("CacheDevice:putOject() - Exiting");
	}
	
	public List<Device> getAllDevicesForUser(int user_id)
	{
		List<Device> list = new ArrayList<Device>();
		
        // Looping through the HashMap to return list of user devices
        // Using for-each loop
        for (Map.Entry item : cache.entrySet())
        {
            Device device = ((Device)item.getValue());
            if (device.getUser_id() == user_id)
            {
            	list.add(device);
            }
        }        
		return list;
	}
	
	public List<Device> getAllDevices()
	{
		ArrayList<Device> list = cache.values().stream().collect(Collectors.toCollection(ArrayList::new));
		
		return list;
	}
}
