package business;

import java.util.List;

import javax.ejb.Local;
import javax.ejb.LocalBean;
import javax.interceptor.Interceptors;

import beans.Role;
import data.DatabaseInterface;
import data.DatabaseService;
import interceptors.LoggingInterceptor;

/**
 * ---------------------------------------------------------------
 * Name      : Group 1 (Kelly Lamb, Brian Cantrell)
 * Date      : 2022-02-14
 * Class     : CST-361 Design Patterns with Java
 * Professor : Mohamed Mneimneh
 * Assignment: Milestone (Motion Devices)
 * Disclaimer: This is our own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Class to handle all business processes for roles
 * 2. 
 * 3.
 * ---------------------------------------------------------------
 */

@Local(RoleBusinessInterface.class)
@LocalBean
@Interceptors(LoggingInterceptor.class)
public class RoleBusinessService implements RoleBusinessInterface
{
    private final DatabaseInterface db = new DatabaseService();

	/**
	 * Default Constructor
	 */
	public RoleBusinessService() {}

	@Override
	public List<Role> readAllRoles()
	{
		return db.readAllRoles();
	}

	@Override
	public boolean createRole(Role role)
	{
		return db.createRole(role);
	}

	@Override
	public Role selectRoleById(int id)
	{
		return db.selectRoleById(id);
	}

	@Override
	public boolean deleteRole(Role role)
	{
		return db.deleteRole(role);
	}

	@Override
	public boolean updateRole(Role role)
	{
		return db.updateRole(role);
	}
}
