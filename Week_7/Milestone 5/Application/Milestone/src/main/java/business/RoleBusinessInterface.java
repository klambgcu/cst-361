package business;

import java.util.List;

import beans.Role;

/**
 * ---------------------------------------------------------------
 * Name      : Group 1 (Kelly Lamb, Brian Cantrell)
 * Date      : 2022-02-14
 * Class     : CST-361 Design Patterns with Java
 * Professor : Mohamed Mneimneh
 * Assignment: Milestone (Motion Devices)
 * Disclaimer: This is our own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Interface to house all business processes for roles
 * 2. 
 * 3.
 * ---------------------------------------------------------------
 */

public interface RoleBusinessInterface
{
    /**
     * 
     * @return List : All roles from the database
     */
    public List<Role> readAllRoles();
    
    /**
     * 
     * @param role : Role to insert into the database
     * @return boolean: true(successful), false otherwise
     */
    public boolean createRole(Role role);
    
    /**
     * 
     * @param id: Integer ID (Key) to select role
     * @return Role: User object if found otherwise null
     */
    public Role selectRoleById(int id);
    
    /**
     * 
     * @param role : Role model to delete from database (ID)
     * @return boolean: true(successful), false otherwise
     */
    public boolean deleteRole(Role role);
    
    /**
     * 
     * @param role : Role object to update
     * @return boolean: true(successful), false otherwise
     */
    public boolean updateRole(Role role);

}
