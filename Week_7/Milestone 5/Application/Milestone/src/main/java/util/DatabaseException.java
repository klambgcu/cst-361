package util;

/**
 * ---------------------------------------------------------------
 * Name      : Group 1 (Kelly Lamb, Brian Cantrell)
 * Date      : 2022-02-14
 * Class     : CST-361 Design Patterns with Java
 * Professor : Mohamed Mneimneh
 * Assignment: Milestone (Motion Devices)
 * Disclaimer: This is our own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Custom Exceptions - Convert to Runtime Exception
 * 2. Extend as necessary
 * 3.
 * ---------------------------------------------------------------
 */
public class DatabaseException extends RuntimeException
{
	private static final long serialVersionUID = 8749912411505566655L;
	
	public DatabaseException(Throwable e)
	{
		super(e);
	}
}
