package data;

import java.sql.Connection;
import java.sql.DriverManager;

/**
 * ---------------------------------------------------------------
 * Name      : Group 1 (Kelly Lamb, Brian Cantrell)
 * Date      : 2022-02-14
 * Class     : CST-361 Design Patterns with Java
 * Professor : Mohamed Mneimneh
 * Assignment: Milestone (Motion Devices)
 * Disclaimer: This is our own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Class defining database configuration - obtain connection
 * 2.
 * 3.
 * ---------------------------------------------------------------
 */

public class DatabaseConfig
{
    private final static String DB_DRIVER = "com.mysql.cj.jdbc.Driver";
    private final static String DB_URL = "jdbc:mysql://localhost:3306/cst-361";
    private final static String DB_USER = "root";
    private final static String DB_PASS = "root";

    /**
    *
    * @return connection : a helper method to return a connection to the database
    */
   protected Connection getConnection() throws Exception
   {
       Connection connection = null;

       Class.forName(DB_DRIVER);
       connection = DriverManager.getConnection(DB_URL, DB_USER, DB_PASS);

       return connection;
   }
}
