package data;

import java.util.List;

import javax.ejb.Local;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;

import beans.Console;
import beans.Device;
import beans.Motion;
import beans.Role;
import beans.User;
import interceptors.LoggingInterceptor;

/**
 * ---------------------------------------------------------------
 * Name      : Group 1 (Kelly Lamb, Brian Cantrell)
 * Date      : 2022-02-14
 * Class     : CST-361 Design Patterns with Java
 * Professor : Mohamed Mneimneh
 * Assignment: Milestone (Motion Devices)
 * Disclaimer: This is our own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Service to handle all database CRUD for exposed methods
 * 2. 
 * 3.
 * ---------------------------------------------------------------
 */

/**
 * Session Bean implementation
 */
@Stateless
@Local(DatabaseInterface.class)
@LocalBean
@Interceptors(LoggingInterceptor.class)
public class DatabaseService implements DatabaseInterface
{
	//@EJB
	//UserDAOInterface<User> userDAO;
    private UserDAO userDAO = new UserDAO();
	
	//@EJB
	//RoleDAO roleDAO;
    private RoleDAO roleDAO = new RoleDAO();

	//@EJB
	//DeviceDAO deviceDAO;
	private DeviceDAO deviceDAO = new DeviceDAO();

	//@EJB
	//MotionDAO motionDAO;
	private MotionDAO motionDAO = new MotionDAO();

	//@EJB
	//ConsoleDAO consoleDAO;
	private ConsoleDAO consoleDAO = new ConsoleDAO();
    
	/**
	 * Default Constructor
	 */
	public DatabaseService() {}

    /**
     * 
     * @return List : All users from the database
     */
	@Override
    public List<User> readAllUsers()
	{
        return userDAO.findAll();
    }

    /**
     * 
     * @param user : User to insert into the database
     * @return boolean: true(successful), false otherwise
     */
	@Override
    public boolean createUser(User user)
	{
        return userDAO.create(user);
    }
    
	   /**
     * 
     * @param id: Integer ID (Key) to select user
     * @return User: User object if found otherwise null
     */
	@Override
    public User selectUserById(int id)
	{
        return userDAO.findById(id);
    }
    
    /**
     * 
     * @param user : User model to delete from database (ID)
     * @return boolean: true(successful), false otherwise
     */
	@Override
    public boolean deleteUser(User user)
	{
        return userDAO.delete(user);
    }
    
    /**
     * 
     * @param user : User object to update
     * @return boolean: true(successful), false otherwise
     */
	@Override
	public boolean updateUser(User user)
	{
        return userDAO.update(user);
    }
    
    /**
     * 
     * @param email : String name to search for user
     * @param password : String name to search for user
     * @return User: User object if found otherwise null
     */
	@Override
    public User selectUserByEmailPassword(String email, String password)
    {
		return userDAO.getByEmailPassword(email, password);
    }
    
    /**
     * 
     * @param email : email should be unique - verify
     * @return boolean: true(exists), false otherwise
     */
	@Override
    public boolean doesEmailExist(String email)
    {
		return userDAO.emailExists(email);
    }
    
	
    //-----------------------------------------------------
    // All exposed methods for the Role DAO
    //-----------------------------------------------------
	
    /**
     * 
     * @return List : All roles from the database
     */
	@Override
	public List<Role> readAllRoles()
	{
		return roleDAO.findAll();
	}

    /**
     * 
     * @param role : Role to insert into the database
     * @return boolean: true(successful), false otherwise
     */
	@Override
	public boolean createRole(Role role)
	{
		return roleDAO.create(role);
	}

    /**
     * 
     * @param id: Integer ID (Key) to select role
     * @return Role: User object if found otherwise null
     */
	@Override
	public Role selectRoleById(int id)
	{
		return roleDAO.findById(id);
	}

    /**
     * 
     * @param role : Role model to delete from database (ID)
     * @return boolean: true(successful), false otherwise
     */
	@Override
	public boolean deleteRole(Role role)
	{
		return roleDAO.delete(role);
	}

    /**
     * 
     * @param role : Role object to update
     * @return boolean: true(successful), false otherwise
     */
	@Override
	public boolean updateRole(Role role)
	{
		return roleDAO.update(role);
	}
	
    //-----------------------------------------------------
    // All exposed methods for the Device DAO
    //-----------------------------------------------------
	
    /**
     * 
     * @return List : All devices from the database
     */
	@Override
	public List<Device> readAllDevices()
	{
		return deviceDAO.findAll();
	}

    /**
     * 
     * @param device : Device to insert into the database
     * @return boolean: true(successful), false otherwise
     */
	@Override
	public boolean createDevice(Device device)
	{
		return deviceDAO.create(device);
	}

    /**
     * 
     * @param id: Integer ID (Key) to select device
     * @return Device: User object if found otherwise null
     */
	@Override
	public Device selectDeviceById(int id)
	{
		return deviceDAO.findById(id);
	}

    /**
     * 
     * @param device : Device model to delete from database (ID)
     * @return boolean: true(successful), false otherwise
     */
	@Override
	public boolean deleteDevice(Device device)
	{
		return deviceDAO.delete(device);
	}

    /**
     * 
     * @param device : Device object to update
     * @return boolean: true(successful), false otherwise
     */
	@Override
	public boolean updateDevice(Device device)
	{
		return deviceDAO.update(device);
	}

    //-----------------------------------------------------
    // All exposed methods for the Motion DAO
    //-----------------------------------------------------
	
    /**
     * 
     * @return List : All motions from the database
     */
	@Override
	public List<Motion> readAllMotions()
	{
		return motionDAO.findAll();
	}

    /**
     * 
     * @param motion : Motion to insert into the database
     * @return boolean: true(successful), false otherwise
     */
	@Override
	public boolean createMotion(Motion motion)
	{
		return motionDAO.create(motion);
	}

    /**
     * 
     * @param id: Integer ID (Key) to select motion
     * @return Motion: User object if found otherwise null
     */
	@Override
	public Motion selectMotionById(int id)
	{
		return motionDAO.findById(id);
	}

    /**
     * 
     * @param motion : Motion model to delete from database (ID)
     * @return boolean: true(successful), false otherwise
     */
	@Override
	public boolean deleteMotion(Motion motion)
	{
		return motionDAO.delete(motion);
	}

    /**
     * 
     * @param motion : Motion object to update
     * @return boolean: true(successful), false otherwise
     */
	@Override
	public boolean updateMotion(Motion motion)
	{
		return motionDAO.update(motion);
	}

    //-----------------------------------------------------
    // All exposed methods for the Console DAO
    // A specialized DAO to handle communication to the 
    // console application.
    //-----------------------------------------------------
    /**
     * 
     * @return List : All users that have devices assigned and activated database
     */
	@Override
	public List<Console> getUsersWithDevices()
	{
		return consoleDAO.getUsersWithDevices();
	}

    /**
     * @param user_id : Integer id for user
     * @return List : All activated devices for specific user from the database
     */
	@Override
	public List<Console> getUsersWithDevices(int user_id)
	{
		return consoleDAO.getUsersWithDevices(user_id);
	}
}
