package com.motiondevices;

import java.util.ArrayList;
import java.util.List;
import java.sql.*;

public class UserDataList implements DataList
{
	private List<DTO> list = new ArrayList<DTO>();
	private DataListCallAPI api = new DataListCallAPI();

	public UserDataList()
	{
		ResponseDataModel data = api.callAPI("http://localhost:8080/Milestone/rest/console/getusers");

		for (Console item : data.getData())
		{
			//System.out.println(item.getId() + item.getDisplayName());
			list.add( new DTO( item.getId(), item.getDisplayName() ) );
		}

		/*******************************************************
		Replace SQL with API Calls

		try
		{
			Class.forName("com.mysql.cj.jdbc.Driver");
			Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/cst-361","root","root");
			PreparedStatement preparedStatement = connection.prepareStatement("select u.id, u.firstname as name, count(1) as amount from users u, devices d where u.role_id = 1 and u.id = d.user_id and d.activated = true group by u.id, u.firstname order by u.id");

			ResultSet rs = preparedStatement.executeQuery();

			while (rs.next())
			{
				int user_id = rs.getInt("id");
				String name = rs.getString("name");
				int amount  = rs.getInt("amount");
				String displayName = name + " (" + amount + ")";
				list.add( new DTO(user_id, displayName) );
			}

			connection.close();
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
		********************************************************/
	}

	public List<DTO> getList()
	{
		return this.list;
	}
}
