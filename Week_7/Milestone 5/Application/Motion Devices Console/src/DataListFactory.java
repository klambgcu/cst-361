package com.motiondevices;

import java.util.List;

public class DataListFactory
{
	public List<DTO> getDataList(String what, int id)
	{
		DataList obj = null;

		switch (what.toLowerCase())
		{
			case "users": obj = new UserDataList(); break;
			case "devices": obj =  new DeviceDataList(id); break;
		}

		return obj.getList();
	}
}
