package com.motiondevices;

import java.util.List;

/**
 * ---------------------------------------------------------------
 * Name      : Group 1 (Kelly Lamb, Brian Cantrell)
 * Date      : 2022-02-21
 * Class     : CST-361 Design Patterns with Java
 * Professor : Mohamed Mneimneh
 * Assignment: Milestone (Motion Devices)
 * Disclaimer: This is our own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Class defining a DTO for the console
 * 2. Used for shortened (facade) version of user and device info
 * 3.
 * ---------------------------------------------------------------
 */

public class ResponseDataModel extends ResponseModel
{
	private List<Console> data;

	/**
	 * Default Constructor
	 */
	public ResponseDataModel() {}

	/**
	 * Non-Default Constructor
	 * @param status integer for numeric response
	 * @param message String response for status
	 * @param data list of console information
	 */
	public ResponseDataModel(int status, String message, List<Console> data)
	{
		super(status, message);
		this.data = data;
	}

	/**
	 * @return the data
	 */
	public List<Console> getData()
	{
		return data;
	}

	/**
	 * @param data the data to set
	 */
	public void setData(List<Console> data)
	{
		this.data = data;
	}
}
