package controllers;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import beans.Album;

/**
 * ---------------------------------------------------------------
 * Name      : Kelly Lamb
 * Date      : 2022-02-11
 * Class     : CST-361 Design Patterns with Java
 * Professor : Mohamed Mneimneh
 * Assignment: Activity 3
 * Disclaimer: This is our own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Class defining a controller for album management
 * 2.
 * 3.
 * ---------------------------------------------------------------
 */
@ManagedBean
@ViewScoped
public class AlbumController
{
	public String onSubmit(Album album) 
	{
		FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("album", album);
        
        return "AddAlbumResponse.xhtml";
	}
}
