package business;

import java.util.HashMap;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;

import beans.Album;

/**
 * ---------------------------------------------------------------
 * Name      : Kelly Lamb
 * Date      : 2022-03-07
 * Class     : CST-361 Design Patterns with Java
 * Professor : Mohamed Mneimneh
 * Assignment: Activity 7b
 * Disclaimer: This is our own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Concrete class to handle album track information finding
 * 2.
 * 3.
 * ---------------------------------------------------------------
 */

@Singleton
@Startup
public class Cache
{
	private HashMap<String, Album> cache;

	public Cache() {}
	
	@PostConstruct
	public void init()
	{
		cache = new HashMap<String, Album>();
	}

	public Album getObject(Album album)
	{
		System.out.println("Cache:getObject() - Entering");
		
		String key = album.getArtist().trim() + album.getTitle().trim() + Integer.toString(album.getYear()).trim();
		System.out.println("Cache:getObject() - key: " + key);
		
		Album model = cache.get(key);
		String msg = (null == model) ? "Item not in the cache." : "Item found in cache.";
		System.out.println("Cache:getObject() - " + msg);
		
		System.out.println("Cache:getObject() - Exiting");
		return model;
	}

	public void putObject(Album album)
	{
		System.out.println("Cache:putOject() - Entering");
		String key = album.getArtist().trim() + album.getTitle().trim() + Integer.toString(album.getYear()).trim();
		
		System.out.println("Cache:putOject() - Storing or replacing album in cache - key: " + key);
		cache.put(key, album);
		
		System.out.println("Cache:putOject() - Exiting");
	}
}
