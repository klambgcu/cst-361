package beans;

/**
 * ---------------------------------------------------------------
 * Name      : Kelly Lamb
 * Date      : 2022-02-21
 * Class     : CST-361 Design Patterns with Java
 * Professor : Mohamed Mneimneh
 * Assignment: Activity 6
 * Disclaimer: This is our own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Class defining a response DTO
 * 2.
 * 3.
 * ---------------------------------------------------------------
 */

public class ResponseModel
{
	private int status;
	private String message;
	
	public ResponseModel() {}

	/**
	 * @param status
	 * @param message
	 */
	public ResponseModel(int status, String message)
	{
		this.status = status;
		this.message = message;
	}

	/**
	 * @return the status
	 */
	public int getStatus()
	{
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(int status)
	{
		this.status = status;
	}

	/**
	 * @return the message
	 */
	public String getMessage()
	{
		return message;
	}

	/**
	 * @param message the message to set
	 */
	public void setMessage(String message)
	{
		this.message = message;
	}
}
